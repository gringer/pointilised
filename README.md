This is a font constructed from primitive curves (lines, arcs), with parametric thickness settings.

How to create the pointilised TTF files:

1. run `./makePointilisedGlyph.py`
1. open up the generated fonts (`.sfd` extension) in FontForge
1. Select all (`<ctrl> + A`)
1. Element -> Overlap -> Remove overlap
1. Element -> Add Extrema
1. Element -> Round -> To Int
1. Hints -> AutoHint
1. File -> Generate Fonts
