#!/usr/bin/env python3

from math import ceil, cos, sin, tan, pi, sqrt, atan
from datetime import date
from functools import reduce
import sys
import unicodedata
from collections import defaultdict

global fontMode
global gapSize
global roundEnds
global maxID
maxID = 0x2023
fontMode = "kern"
gapSize = 1
roundEnds = True
doKerning = True
gwLookup = [6.5] * 8300
# generic glyphs within ranges
for gi in range(len(gwLookup)):
  if (gi < 65) :
    gwLookup[gi] = 6.5
  if ((gi >= 65) and (gi < 127)) :
    if (gi < 97) :
      gwLookup[gi] = 8
    elif (gi < 127) :
      gwLookup[gi] = 6
# specific cases (in order of width)
for gi in [39, 44, 46, 8216, 8217]:  ## ' , . I <left single quote> <right single quote>
  gwLookup[gi] = 1
for gi in [33, 58, 59, 73, 105, 124]: ## ! : ; i |
  gwLookup[gi] = 2
for gi in [91, 93, 96, 108, 116, 123, 125, 207, 298, ## [ ] ` l t { } Ï Ī
           239, 299, 0x2022]: ## ï ī <bullet>
  gwLookup[gi] = 3
for gi in [32, 34, 40, 41, 8220, 8221]: ## <space> " ( )
  gwLookup[gi] = 3.5
for gi in [106]: ## j <left double quote> <right double quote>
  gwLookup[gi] = 4
for gi in [102, 114]: ## f r
  gwLookup[gi] = 4.5
for gi in [63, 107, 115]: ## <question mark> k s
  gwLookup[gi] = 5
for gi in [45, 69, 70, 74, 76, 94, 203, 274, 235, 275, 228, 257]: ## E F L J ^ Ë Ē ë ē ä ā
  gwLookup[gi] = 6
for gi in [38, 92, 100, 117, 126, 246, 333, 252, 363, 0x2013]: ## & \ d u ö ō ü ū <n-dash>
  gwLookup[gi] = 6.5
for gi in [64, 65, 66, 67, 68, 71, 72, 75, 78, 80, 82, 83, 85, ## @ A B C D G H K N P R S U
           86, 88, 89, 90, 95, 196, 256, 214, 332, 220, 362, 223]: ## V X Y Z Ä Ā Ö Ō Ü Ū ß
  gwLookup[gi] = 7
for gi in [84, 87, 109, 119, 0x2014]: ## T W m w <m-dash>
  gwLookup[gi] = 8

def gW(val):
  return(gwLookup[val])

#def sumLwds(A,start,end,value=0) =
#  reduce(lambda x, y: x + gW[y], A[start:end], 0)

def makePolyLine(A):
  if(fontMode == "kern"):
    # Start off with the "left side" of the shadow at the right (+1),
    # and the "right side" of the shadow at the left (-1)
    resL = [81] * 101
    resR = [-1] * 101
    for pi in range(len(A)):
      p1 = A[pi]
      p2 = A[(pi + 1) % len(A)]
      ySpan = p2[1] - p1[1]
      xSpan = p2[0] - p1[0]
      if(ySpan == 0):
        py = int(p1[1] * 10 + 0.5)
        px1 = int(p1[0] * 10 + 0.5)
        px2 = int(p2[0] * 10 + 0.5)
        if((py >= 0) and (py <= 100)):
          resL[py] = max(0, min(resL[py], px1, px2))
          resR[py] = min(85, max(resR[py], px1, px2))
      else:
        for pyi in frange(0, ySpan, 0.1 * (ySpan / abs(ySpan))):
          py = int((p1[1] + (pyi / ySpan) * ySpan) * 10 + 0.5)
          px = int((p1[0] + (pyi / ySpan) * xSpan) * 10 + 0.5)
          if((py >= 0) and (py <= 100)):
            resL[py] = max(0, min(resL[py], px))
            resR[py] = min(85, max(resR[py], px))
    return([[resL, resR]])
  if(fontMode == "outline"):
    A[0][0] += gapSize/2
    res = [("  %0.0f %0.0f m 1" % (A[0][0] * (1576  / 10), A[0][1] * (1576 / 10)))]
    for lp in A[1:]:
      lp[0] += gapSize/2
      res += [("    %0.0f %0.0f l 1" % (lp[0] * (1576 / 10), lp[1] * (1576 / 10)))]
    res += [("    %0.0f %0.0f l 1" % (A[0][0] * (1576 / 10), A[0][1] * (1576 / 10)))]
    return(res)

def getHWidth(w, h, th):
  ## Magic diagonal width calculation, provided by Twitter/@LBelenky
  ## https://twitter.com/LBelenky/status/1428887569660628993
  return( (h * th * sqrt(h**2 - th**2 + w**2) - th**2 * w) /
          (h**2 - th**2) )

def getRoundedAngle(w, h, th, half = False, radians=False):
  ## Determines the angle for the rounded bottom of a vDiagLine
  Wh = getHWidth(w * (2 if half else 1), h * (2 if half else 1), th) / (2 if half else 1)
  if(Wh == th):
    return(th/2)
  if(radians):
    return(atan((w-Wh) / h))
  else:
    return(atan((w-Wh) / h) / pi * 180)

def getRoundedDist(w, h, th, half = False):
  ## Determines the horizontal displacement for the rounded bottom of a vDiagLine
  if(w == 0):
    return(th/2)
  Wh = getHWidth(w * (2 if half else 1), h * (2 if half else 1), th) / (2 if half else 1)
  theta = atan((w-Wh) / h)
  return(th/2 * (cos(theta) + tan(theta)*sin(theta) + tan(theta)))

def vDiagLineTo(start, end, th, reverse=False,
                halfEnd=False, halfStart=False, roundEnd = 0):
  w = end[0] - start[0]
  if(reverse):
    h = start[1] - end[1]
  else:
    h = end[1] - start[1]
  if(halfStart or halfEnd):
    x = getHWidth(w*2, h*2, th)
  else :
    x = getHWidth(w, h, th)
  sofs = eofs = 0
  if(halfEnd):
    eofs = -x/2
  if(halfStart):
    sofs = -x/2
  p1 = [end[0]-(x+eofs), end[1]]
  p2 = [end[0]-(eofs), end[1]]
  p3 = [start[0]+x+sofs, start[1]]
  p4 = [start[0]+sofs, start[1]]
  yMul = (1 if (end[1] > start[1]) else -1)
  addEnd = []
  if((roundEnd & 1) == 1):
    xOfs = getRoundedDist(w, h, th, half=(halfStart or halfEnd))
    yOfs = th / 2
    roundAng = getRoundedAngle(w, h, th, half=(halfStart or halfEnd), radians=True)
    if (start[0] < end[0]):
      p3 = [start[0] + xOfs + th/2 * cos(roundAng),
            start[1] + (th/2 - th/2 * sin(roundAng)) * yMul]
      p4 = [start[0] + xOfs - th/2 * cos(roundAng),
            start[1] + (th/2 + th/2 * sin(roundAng)) * yMul]
      addEnd += arc([start[0] + xOfs, start[1] + (th/2 * yMul)],
                    th, th, 0, 360, th)
    else:
      p3 = [start[0] + x - xOfs - th/2 * cos(roundAng),
            start[1] + (th/2 - th/2 * sin(roundAng)) * yMul]
      p4 = [start[0] + x - xOfs + th/2 * cos(roundAng),
            start[1] + (th/2 + th/2 * sin(roundAng)) * yMul]
      addEnd += arc([start[0] + x - xOfs, start[1] + (th/2 * yMul)],
                    th, th, 0, 360, th)
  if((roundEnd & 2) == 2):
    xOfs = getRoundedDist(w, h, th, half=(halfStart or halfEnd))
    yOfs = th / 2
    roundAng = getRoundedAngle(w, h, th, half=(halfStart or halfEnd), radians=True)
    if (start[0] < end[0]):
      p1 = [end[0] - xOfs - th/2 * cos(roundAng),
            end[1] - (th/2 - th/2 * sin(roundAng)) * yMul]
      p2 = [end[0] - xOfs + th/2 * cos(roundAng),
            end[1] - (th/2 + th/2 * sin(roundAng)) * yMul]
      addEnd += arc([end[0] - xOfs, end[1] - (th/2 * yMul)],
                    th, th, 0, 360, th)
    else:
      p1 = [end[0] + x + xOfs + th/2 * cos(roundAng),
            end[1] - (th/2 - th/2 * sin(roundAng)) * yMul]
      p2 = [end[0] + x + xOfs - th/2 * cos(roundAng),
            end[1] - (th/2 + th/2 * sin(roundAng)) * yMul]
      addEnd += arc([end[0] + x + xOfs, end[1] - (th/2 * yMul)],
                    th, th, 0, 360, th)
  points = [p1, p2, p3, p4]
  if(reverse):
    points.reverse()
  return(makePolyLine(points) + addEnd)

def vLineTo(start, end, th, roundEnd = 0):
  addEnd = []
  hOfs = th/2 if (start[0] < end[0]) else (-th/2)
  vOfs = th/2 if (start[1] < end[1]) else (-th/2)
  p1 = [end[0]-th, end[1]]
  p2 = [end[0], end[1]]
  p3 = [start[0]+th, start[1]]
  p4 = [start[0], start[1]]
  if((roundEnd & 1) == 1):
    p3[1] += vOfs
    p4[1] += vOfs
    addEnd += arc([start[0] + hOfs, start[1] + vOfs],
                  th, th, 0, 360, th)
  if((roundEnd & 2) == 2):
    p1[1] -= vOfs
    p2[1] -= vOfs
    addEnd += arc([end[0] - hOfs, end[1] - vOfs],
                  th, th, 0, 360, th)
  return(makePolyLine([p1, p2, p3, p4]) + addEnd)

def hLineTo(start, end, th, roundEnd = 0):
  addEnd = []
  hOfs = th/2 if (start[0] < end[0]) else (-th/2)
  vOfs = th/2 if (start[1] < end[1]) else (-th/2)
  p1 = [start[0], start[1]]
  p2 = [start[0], start[1]+th]
  p3 = [end[0], end[1]]
  p4 = [end[0], end[1]-th]
  if((roundEnd & 1) == 1):
    p1[0] += vOfs
    p2[0] += vOfs
    addEnd += arc([start[0] + hOfs, start[1] + vOfs],
                  th, th, 0, 360, th)
  if((roundEnd & 2) == 2):
    p3[0] -= vOfs
    p4[0] -= vOfs
    addEnd += arc([end[0] - hOfs, end[1] - vOfs],
                  th, th, 0, 360, th)
  return(makePolyLine([p1, p2, p3, p4]) + addEnd)

def frange(start, stop, step):
  #print("doing from %0.2f to %0.2f in steps of %0.2f" % (
  #  start, stop, step))
  cur = start
  if((start < stop) and (step > 0)):
    while(cur < stop):
      #print(cur)
      yield cur
      cur += step
  elif((start > stop) and (step < 0)):
    while(cur > stop):
      #print(cur)
      yield cur
      cur += step
  yield stop

def arcBezier(arcCentre, w, h, thFrom, thTo, thickness):
  cf = 1 / 180 * pi ## conversion factor because cos/sin is in radians
  thFrom = thFrom % 360
  thTo = thTo % 360
  if (thTo <= thFrom):
    thTo = (thTo + 360)
  if(fontMode == "kern"):
    ## solve for y on LHS and RHS:
    ##  [x, y] = [w * cos(theta), h * sin(theta)]
    ## work out minimum angle for 0.1 points
    maxCirc = max(w, h) * pi
    degPerPoint = 360 / (maxCirc * 10)
    resL = [81] * 101
    resR = [-1] * 101
    for th in frange(thFrom, thTo, degPerPoint):
      # model outer curve
      px = int((w/2 * cos(th * cf) + arcCentre[0]) * 10 + 0.5)
      py = int((h/2 * sin(th * cf) + arcCentre[1]) * 10 + 0.5)
      if((py >= 0) and (py <= 100)):
        resL[py] = max(0, min(px, resL[py]))
        resR[py] = min(81, max(px, resR[py]))
      # model inner curve
      if(thickness < min(w, h)):
        px = int(((w/2 - thickness) * cos(th * cf) + arcCentre[0]) * 10 + 0.5)
        py = int(((h/2 - thickness) * sin(th * cf) + arcCentre[1]) * 10 + 0.5)
        if((py >= 0) and (py <= 100)):
          resL[py] = max(0, min(px, resL[py]))
          resR[py] = min(85, max(px, resR[py]))
    if((thTo - thFrom) < 360):
      # model connecting lines (based on polyline algorithm)
      for th in [thFrom, thTo]:
        p1 = [int((w/2 * cos(th * cf) + arcCentre[0]) * 10 + 0.5),
              int((h/2 * sin(th * cf) + arcCentre[1]) * 10 + 0.5)]
        p2 = [int(((w/2 - thickness) * cos(th * cf) + arcCentre[0]) * 10 + 0.5),
              int(((h/2 - thickness) * sin(th * cf) + arcCentre[1]) * 10 + 0.5)]
        ySpan = p2[1] - p1[1]
        xSpan = p2[0] - p1[0]
        if(ySpan == 0):
          py = p1[1]
          px1 = p1[0]
          px2 = p2[0]
          if((py >= 0) and (py <= 100)):
            resL[py] = max(0, min(resL[py], px1, px2))
            resR[py] = min(85, max(resR[py], px1, px2))
        else:
            for pyi in frange(0, ySpan, abs(ySpan) / ySpan):
              py = int(p1[1] + pyi + 0.5)
              px = p1[0] + (pyi / ySpan) * xSpan
              if((py >= 0) and (py <= 100)):
                resL[py] = max(0, min(resL[py], px))
                resR[py] = min(85, max(resR[py], px))
    return([[resL, resR]])
  if(fontMode == "outline"):
    arcCentre[0] += gapSize/2
    steps = ceil(abs(thTo - thFrom) / 15)
    if((thTo - thFrom) == 360):
      steps = 8
    elif((thTo - thFrom) == 180):
      steps = 4
    elif(((thTo - thFrom) % 45 == 0)):
      steps = ((thTo - thFrom) / 45) * 2
    elif(steps < 5):
      steps = ceil(abs(thTo - thFrom) / 5)
    # equivalent number of steps in a full circle
    fcSteps = steps * 360 / (thTo - thFrom)
    th = thickness/2
    wr = w/2
    hr = h/2
    # control point distance based on radius
    # https://stackoverflow.com/a/27863181/3389895
    cpDistW = (4 / 3) * tan(pi / (2*fcSteps)) * wr
    cpDistH = (4 / 3) * tan(pi / (2*fcSteps)) * hr
    cpDistWi = (4 / 3) * tan(pi / (2*fcSteps)) * (wr - thickness)
    cpDistHi = (4 / 3) * tan(pi / (2*fcSteps)) * (hr - thickness)
    incAng = (thTo - thFrom) / steps
    fullCircle = ((thTo % 360) == (thFrom % 360)) ## different behaviour for full circles
    thSteps = list(frange(thTo, thFrom, -incAng))
    ## make an enclosed circle if the arc diameter is smaller than the thickness
    if(fullCircle):
      cpDeltas = [[cpDistW * cos((theta + 90) * cf),
                   cpDistH * sin((theta + 90) * cf)] for theta in thSteps]
      arcPoss = [[(wr) * cos(theta * cf) + arcCentre[0],
                  (hr) * sin(theta * cf) + arcCentre[1]] for theta in thSteps]
      rel = ["  %0.0f %0.0f m 0" % ((arcPoss[0][0]) * (1576 / 10), arcPoss[0][1] * (1576 / 10))]
      cpDeltaLast = cpDeltas[0]
      arcPosLast = arcPoss[0]
      for cpi in range(1, len(arcPoss)):
        arcPos = arcPoss[cpi]
        cpDelta = cpDeltas[cpi]
        rel += ["    %0.0f %0.0f" % ((arcPosLast[0] - cpDeltaLast[0]) * (1576 / 10),
                                     (arcPosLast[1] - cpDeltaLast[1]) * (1576 / 10)) +
                " %0.0f %0.0f" % ((arcPos[0] + cpDelta[0]) * (1576 / 10),
                                  (arcPos[1] + cpDelta[1]) * (1576 / 10)) +
                " %0.0f %0.0f c 0" % (arcPos[0] * (1576 / 10), arcPos[1] * (1576 / 10))]
        cpDeltaLast = cpDelta
        arcPosLast = arcPos
      if((thickness < wr) or (thickness < hr)):
        ## cut out the inner circle
        arcPoss = [[(wr - thickness) * cos(theta * cf) + arcCentre[0],
                    (hr - thickness) * sin(theta * cf) + arcCentre[1]] for theta in thSteps]
        cpDeltas = [[cpDistWi * cos((theta + 90) * cf),
                     cpDistHi * sin((theta + 90) * cf)] for theta in thSteps]
        rel += ["  %0.0f %0.0f m 0" % ((arcPoss[-1][0]) * (1576 / 10), arcPoss[-1][1] * (1576 / 10))]
        cpDeltaLast = cpDeltas[-1]
        arcPosLast = arcPoss[-1]
        for cpi in range(len(arcPoss)-2, -1, -1):
          arcPos = arcPoss[cpi]
          cpDelta = cpDeltas[cpi]
          rel += ["    %0.0f %0.0f" % ((arcPosLast[0] + cpDeltaLast[0]) * (1576 / 10),
                                       (arcPosLast[1] + cpDeltaLast[1]) * (1576 / 10)) +
                  " %0.0f %0.0f" % ((arcPos[0] - cpDelta[0]) * (1576 / 10),
                                    (arcPos[1] - cpDelta[1]) * (1576 / 10)) +
                  " %0.0f %0.0f c 0" % (arcPos[0] * (1576 / 10), arcPos[1] * (1576 / 10))]
          cpDeltaLast = cpDelta
          arcPosLast = arcPos
      return(rel)
    else: ## create an arc with thickness
      cpDeltas = [[cpDistW * cos((theta + 90) * cf),
                   cpDistH * sin((theta + 90) * cf)] for theta in thSteps]
      arcPoss = [[(wr) * cos(theta * cf) + arcCentre[0],
                  (hr) * sin(theta * cf) + arcCentre[1]] for theta in thSteps]
      rel = ["  %0.0f %0.0f m 1" % ((arcPoss[0][0]) * (1576 / 10), arcPoss[0][1] * (1576 / 10))]
      cpDeltaLast = cpDeltas[0]
      arcPosFirst = arcPoss[0]
      arcPosLast = arcPoss[0]
      for cpi in range(1, len(arcPoss)):
        arcPos = arcPoss[cpi]
        cpDelta = cpDeltas[cpi]
        rel += ["    %0.0f %0.0f" % ((arcPosLast[0] - cpDeltaLast[0]) * (1576 / 10),
                                     (arcPosLast[1] - cpDeltaLast[1]) * (1576 / 10)) +
                " %0.0f %0.0f" % ((arcPos[0] + cpDelta[0]) * (1576 / 10),
                                  (arcPos[1] + cpDelta[1]) * (1576 / 10)) +
                " %0.0f %0.0f c %s" % ((arcPos[0]) * (1576 / 10), arcPos[1] * (1576 / 10),
                                       "0" if (cpi < (len(arcPoss)-1)) else "1")]
        cpDeltaLast = cpDelta
        arcPosLast = arcPos
      arcPoss = [[(wr - thickness) * cos(theta * cf) + arcCentre[0],
                  (hr - thickness) * sin(theta * cf) + arcCentre[1]] for theta in thSteps]
      cpDeltas = [[cpDistWi * cos((theta + 90) * cf),
                   cpDistHi * sin((theta + 90) * cf)] for theta in thSteps]
      rel += ["  %0.0f %0.0f l 1" % ((arcPoss[-1][0]) * (1576 / 10), arcPoss[-1][1] * (1576 / 10))]
      arcPosLast = arcPoss[-1]
      cpDeltaLast = cpDeltas[-1]
      for cpi in range(len(arcPoss)-2, -1, -1):
        arcPos = arcPoss[cpi]
        cpDelta = cpDeltas[cpi]
        rel += ["    %0.0f %0.0f" % ((arcPosLast[0] + cpDeltaLast[0]) * (1576 / 10),
                                     (arcPosLast[1] + cpDeltaLast[1]) * (1576 / 10)) +
                " %0.0f %0.0f" % ((arcPos[0] - cpDelta[0]) * (1576 / 10),
                                  (arcPos[1] - cpDelta[1]) * (1576 / 10)) +
                " %0.0f %0.0f c %s" % ((arcPos[0]) * (1576 / 10), arcPos[1] * (1576 / 10),
                                       "0" if (cpi > 0) else "1")]
        cpDeltaLast = cpDelta
        arcPosLast = arcPos
      rel += ["  %0.0f %0.0f l 1" % ((arcPosFirst[0]) * (1576 / 10), arcPosFirst[1] * (1576 / 10))]
      return(rel)

def arc(arcCentre, w, h, thFrom, thTo, thickness, roundEnd=0):
  addEnd = []
  cf = 1 / 180 * pi ## conversion factor because cos/sin is in radians
  if((roundEnd & 1) == 1):
    px = ((w/2-thickness/2) * cos(thFrom*cf) + arcCentre[0])
    py = ((h/2-thickness/2) * sin(thFrom*cf) + arcCentre[1])
    addEnd += arcBezier([px,py], thickness, thickness, 0, 360, thickness/2)
  if((roundEnd & 2) == 2):
    px = ((w/2-thickness/2) * cos(thTo*cf) + arcCentre[0])
    py = ((h/2-thickness/2) * sin(thTo*cf) + arcCentre[1])
    addEnd += arcBezier([px,py], thickness, thickness, 0, 360, thickness/2)
  return(arcBezier(arcCentre, w, h, thFrom, thTo, thickness) + addEnd)

def roundedEnd(arcCentre, thickness, normal, angle=-1, w=0, h=0):
  if(roundEnds):
    if(angle > -1):
      cf = 1 / 180 * pi ## conversion factor because cos/sin is in radians
      px = ((w/2-thickness/2) * cos(angle*cf) + arcCentre[0])
      py = ((h/2-thickness/2) * sin(angle*cf) + arcCentre[1])
      arcCentre = [px, py]
    return(arc(arcCentre, thickness, thickness, normal - 90, normal + 90, thickness/2))
  else:
    return([''])

def fetchSplines(cpId, tk = 1):
  if(cpId == 32): # space
    return([''])
  if(cpId == 33): # !
    return(arc([1, 10-(tk+1)/2], tk*2, tk*2, 0, 180, tk) +
           vDiagLineTo([1,2.5], [1, 10-(tk+1)/2], tk, halfStart=True) +
           vDiagLineTo([1,2.5], [1+tk, 10-(tk+1)/2], tk, halfStart=True) +
           arc([1, 1], tk*1.5, tk*1.5, 0, 360, tk))
  elif(cpId == 34): ## "
    return(vDiagLineTo([0.75-tk/4,7], [0.75, 10], tk/2) +
           vDiagLineTo([0.75-tk/4,7], [0.75+tk/2, 10], tk/2) +
           vDiagLineTo([2.75-tk/4,7], [2.75, 10], tk/2) +
           vDiagLineTo([2.75-tk/4,7], [2.75+tk/2, 10], tk/2))
    return(vLineTo([0.75-tk/4,6], [0.75, 9], tk/2) +
      vLineTo([0.75-tk/4,6], [0.75+tk/2, 9], tk/2) +
      vLineTo([2.75-tk/4,6], [2.75, 9], tk/2) +
      vLineTo([2.75-tk/4,6], [2.75+tk/2, 9], tk/2))
  elif(cpId == 35): ## #
    return(vDiagLineTo([0.5,1], [1.5+tk,9], tk, roundEnd=3) +
      vDiagLineTo([5-tk,1], [6,9], tk, roundEnd=3) +
      hLineTo([0,6.5-tk/2], [6.5,6.5+tk/2], tk, roundEnd=3) +
      hLineTo([0,3.5-tk/2], [6.5,3.5+tk/2], tk, roundEnd=3))
  elif(cpId == 36): ## $
    return(vLineTo([3.5-tk/2,0], [3.5+tk/2,10], tk, roundEnd=3) +
      arc([3.5, 9-(4+tk/2)/2], 6.5, 4+tk/2, 30, 270, tk, roundEnd=1) +
      arc([3.5, 1+(4+tk/2)/2], 6.5, 4+tk/2, 210, 90, tk, roundEnd=1))
  elif(cpId == 37): ## %
    return(arc([1.5, 7.5], 3, 3, 0, 360, tk) +
      arc([5, 2.5], 3, 3, 0, 360, tk) +
      vDiagLineTo([1,1], [5.5,9], tk, roundEnd=3))
  elif(cpId == 38): ## &
    return(arc([6.5, 6.5], 11, 11, 180, 270, tk, roundEnd=2) +
           arc([2.75, 6.5], 3.5, 5, 0, 180, tk) +
           arc([2, 6.5], 5, 3 + 2 * tk, 270, 360, tk) +
           arc([2, 3], 4, 4, 90, 270, tk) +
           arc([2, 6.5], 11, 11, 270, 330, tk, roundEnd=2))
  elif(cpId == 39): ## ' [straight single quote]
    return(vLineTo([0.5-tk/4,7], [0.5, 10], tk/2) +
           vLineTo([0.5-tk/4,7], [0.5+tk/2, 10], tk/2))
  elif(cpId == 40): ## (
    return(arc([3.25, 5], 6.5, 10, 90, 270, tk, roundEnd=3))
  elif(cpId == 41): ## )
    return(arc([0, 5], 6.5, 10, 270, 90, tk, roundEnd=3))
  elif(cpId == 42): ## *
    return(vDiagLineTo([0,7], [6.5,3], tk, reverse=True, roundEnd=3) +
           vDiagLineTo([0,3], [6.5,7], tk, roundEnd=3) +
           vLineTo([3.25-tk/2, 2], [3.25+tk/2, 8], tk, roundEnd=3))
  elif(cpId == 43): ## +
    return(vLineTo([3.25-tk/2,1.75], [3.25+tk/2, 8.25], tk, roundEnd=3) +
      hLineTo([0,5-tk/2], [6.5, 5+tk/2], tk, roundEnd=3))
  elif(cpId == 44): ## ,
    return(arc([0.5, tk], 0.5+tk*1.5, 0.5+tk*1.5, 0, 360, 0.5+tk*1.5) +
           arc([-0.25 - tk * 0.5, tk], 2+tk*2.5, 2+tk*2.5, 290, 0, 0.25+tk*0.25, roundEnd=1))
  elif(cpId == 45): ## -
    return(hLineTo([0.5,3.5-tk/2], [4.5, 3.5+tk/2], tk, roundEnd=3))
  elif(cpId == 46): ## .
    return(arc([0.5, tk], 0.5+tk*1.5, 0.5+tk*1.5, 0, 360, tk*2))
  elif(cpId == 47): ## /
    return(vDiagLineTo([0,0], [6.5, 10], tk, roundEnd=3))
  elif(cpId == 48): ## 0
    return(vLineTo([6.5-tk,3.5], [6.5,6.5], tk) +
      vLineTo([0,3.5], [tk,6.5], tk) +
      hLineTo([0+tk,3.5], [6.5-tk,6.5], tk) +
      arc([3.25, 6.5], 6.5, 7, 0, 180, tk) +
      arc([3.25, 3.5], 6.5, 7, 180, 0, tk))
  elif(cpId == 49): ## 1
    return(vDiagLineTo([1.5-tk,7], [(6.5+tk)/2,10], tk, roundEnd=1) +
           vLineTo([(6.5-tk)/2,0], [(6.5+tk)/2,10], tk) +
           hLineTo([1.5-tk/2,0], [5+tk/2,tk], tk, roundEnd=3))
  elif(cpId == 50): ## 2
    rx = (13 - sqrt(2) * tk)/(4-2 * sqrt(2))
    ry = ((6.75) * sqrt(2)) / 2 + tk/2
    return(hLineTo([tk/2,0], [6.5,tk], tk, roundEnd=2) +
           arc([3.25, 6.75], 6.5, 6.5, 0, 180, tk, roundEnd=2) +
           arc([6.5-rx, 6.75], 2*rx, 2 * ry, 315, 360, tk) +
           arc([rx, 0], 2*rx, 2 * ry, 135, 180, tk))
  elif(cpId == 51): ## 3
    return(arc([3.25, 10-(10+tk)/4], 6.5, (10+tk)/2, 270, 180, tk, roundEnd=2) +
           hLineTo([3,5-tk/2], [3.5,5+tk/2], tk) +
           arc([3.25, (10+tk)/4], 6.5, (10+tk)/2, 180, 90, tk, roundEnd=1) +
           roundedEnd([3, 5], tk, 180))
  elif(cpId == 52): ## 4
    return(vLineTo([5-tk,0], [5,10], tk, roundEnd=1) +
           hLineTo([0,3.5-tk/2], [6.5,3.5+tk/2], tk, roundEnd=2) +
           vDiagLineTo([0,3.5+tk/2], [5,10], tk))
  elif(cpId == 53): ## 5
    return(vLineTo([0,6.5], [tk,10-tk], tk) +
           arc([tk, 6.5], tk*2, tk*2, 180, 270, tk) +
           hLineTo([tk,6.5-tk], [3.25,6.5], tk) +
           hLineTo([0,10-tk], [6,10], tk, roundEnd=2) +
           arc([3.25, 6.5/2], 6.5, 6.5, 180, 90, tk, roundEnd=1))
  elif(cpId == 54): ## 6
    return(arc([5, 3], 10, (10-3)*2, 90, 180, tk, roundEnd=1) +
           arc([3.25, 3], 6.5, 6, 0, 360, tk))
  elif(cpId == 55): ## 7
    return(hLineTo([0,10-tk], [6.5,10], tk, roundEnd=1) +
           vDiagLineTo([2,0], [6.5,10-tk], tk, roundEnd=1))
  elif(cpId == 56): ## 8
    return(arc([3.25, 10-(10+tk)/4], 6.5, (10+tk)/2, 0, 360, tk) +
      arc([3.25, (10+tk)/4], 6.5, (10+tk)/2, 0, 360, tk))
  elif(cpId == 57): ## 9
    return(vDiagLineTo([6.5-tk*2,0], [6.5,7], tk, roundEnd=1) +
           arc([3.25, 7], 6.5, 6, 0, 360, tk))
  elif(cpId == 58): ## :
    return(arc([1,1.5], 0.5 + tk*1.5, 0.5 + tk*1.5, 0, 360, 0.5 + tk*1.5) +
           arc([1,5.5], 0.5 + tk*1.5, 0.5 + tk*1.5, 0, 360, 0.5 + tk*1.5))
  elif(cpId == 59): ## ;
    return(arc([1,5.5], 0.5 + tk*1.5, 0.5 + tk*1.5, 0, 360, 0.5 + tk*1.5) +
           arc([1,1.5], 0.5 + tk*1.5, 0.5 + tk*1.5, 0, 360, 0.5 + tk*1.5) +
           arc([0.25 - tk * 0.5, 1.5], 2 + tk*2.5, 2 + tk*2.5, 290, 0, 0.25 + tk*0.25, roundEnd=1))
  elif(cpId == 60): ## <
    hw = getHWidth(5.5, 5, tk)
    return(vDiagLineTo([0.5,5], [6,9], tk, roundEnd=2) +
           vDiagLineTo([0.5,5], [6,1], tk, reverse=True, roundEnd=2))
  elif(cpId == 61): ## =
    return(hLineTo([0,6.5-tk/2], [6.5,6.5+tk/2], tk, roundEnd=3) +
           hLineTo([0,3.5-tk/2], [6.5,3.5+tk/2], tk, roundEnd=3))
  elif(cpId == 62): ## >
    return(vDiagLineTo([0.5,9], [6,5], tk, reverse=True, roundEnd=1) +
           vDiagLineTo([0.5,1], [6,5], tk, roundEnd=1))
  elif(cpId == 63): ## ?
    r = (-sqrt(2) * tk + tk + 5)/(4-2 * sqrt(2))
    dy = (r-tk/2) * (2/sqrt(2))
    return(arc([2.5, 7.5], 5, 5, 0, 180, tk, roundEnd=2) +
      arc([5-r, 7.5], 2*r, 2*r, 315, 360, tk) +
      arc([(2.5-tk/2)+r, 7.5-dy], 2*r, 2*r, 135, 180, tk, roundEnd=2) +
      arc([2.5, 1], tk*1.5, tk*1.5, 0, 360, tk))
  elif(cpId == 64): ## @
    return(arc([3.5, 5], 3.5, 4.5, 0, 360, tk) +
           arc([(12.25 - tk)/2, 5], (6.625 - (11.5 - tk)/2)*2, 5, 180, 360, tk) +
           arc([3.5, 5], 7, 8, 0, 290, tk, roundEnd=2))
  elif(cpId == 65): ## A
    ha = 270 - getRoundedAngle(3.5, 10, tk, half=True)
    hd = getRoundedDist(3.5, 10, tk, half=True)
    return(vDiagLineTo([0,0], [3.5,10], tk, halfEnd=True, roundEnd = 1) +
           vDiagLineTo([3.5,10], [7, 0], tk, reverse=True,
                       halfStart=True, roundEnd = 2) +
           hLineTo([(3.5-tk)*(2.5+tk/2)/10+tk/2,2.5],
                   [7-((3.5-tk)*(2.5+tk/2)/10+tk/2),2.5+tk], tk))
  elif(cpId == 66): ## B
    return(vLineTo([0,0], [tk,10], tk) +
      hLineTo([tk/2,0], [4,tk], tk) +
      hLineTo([tk/2,5-tk/2],  [4,5+tk/2], tk) +
      hLineTo([tk/2,10-tk], [4,10], tk) +
      arc([4, 7.5-tk/4], 6, 5+tk/2, 270, 90, tk) +
      arc([4, 2.5+tk/4], 6, 5+tk/2, 270, 90, tk))
  elif(cpId == 67): ## C
    return(vLineTo([0,3.5], [tk,6.5], tk) +
           arc([3.5, 6.5], 7, 7, 0, 180, tk, roundEnd=1) +
           arc([3.5, 3.5], 7, 7, 180, 0, tk, roundEnd=2))
  elif(cpId == 68): ## D
    return(vLineTo([7-tk,3.5], [7,6.5], tk) +
      vLineTo([0,0], [tk,10], tk) +
      hLineTo([tk,10-tk], [3.5, 10], tk) +
      hLineTo([tk,0], [3.5, tk], tk) +
      arc([3.5, 6.5], 7, 7, 0, 90, tk) +
      arc([3.5, 3.5], 7, 7, 270, 0, tk))
  elif(cpId == 69): ## E
    return(vLineTo([0,0], [tk,10], tk) +
           hLineTo([tk/2,0], [6,tk], tk, roundEnd=2) +
           hLineTo([tk/2,5-tk/2],  [4.75,5+tk/2], tk, roundEnd=2) +
           hLineTo([tk/2,10-tk], [6,10], tk, roundEnd=2))
  elif(cpId == 70): ## F
    return(vLineTo([0,0], [tk,10], tk, roundEnd=1) +
           hLineTo([tk/2,5-tk/2],  [4.75,5+tk/2], tk, roundEnd=2) +
           hLineTo([tk/2,10-tk], [6,10], tk, roundEnd=2))
  elif(cpId == 71): ## G
    return(vLineTo([0,3.5], [tk,6.5], tk) +
           hLineTo([3.5, 3.5], [7,3.5+tk], tk, roundEnd=1) +
           arc([3.5, 6.5], 7, 7, 25, 180, tk, roundEnd=1) +
           arc([3.5, 3.5], 7, 7, 180, 0, tk))
  elif(cpId == 72): ## H
    return(vLineTo([0,0], [tk,10], tk, roundEnd=3) +
           vLineTo([7-tk,0], [7,10], tk, roundEnd=3) +
           hLineTo([tk/2,5-tk/2], [7-tk/2,5+tk/2], tk))
  elif(cpId == 73): ## I
    return(vLineTo([1-tk/2,0], [1+tk/2,10], tk, roundEnd=3))
  elif(cpId == 74): ## J
    return(vLineTo([6-tk,3.5], [6,10], tk, roundEnd=2) +
           arc([3, 3.5], 6, 7, 180, 0, tk, roundEnd=1))
  elif(cpId == 75): ## K
    return(vLineTo([0,0], [tk,10], tk, roundEnd=3) +
           vDiagLineTo([0,5], [7,10], tk, roundEnd = 2) +
           vDiagLineTo([0,5], [7,0], tk, reverse=True, roundEnd = 2))
  elif(cpId == 76): ## L
    return(vLineTo([0,0], [tk,10], tk, roundEnd=2) +
           hLineTo([tk/2,0], [6,tk], tk, roundEnd=2))
  elif(cpId == 77): ## M
    hw = getHWidth(8, 12, tk)
    return(vLineTo([0,0], [tk,10], tk, roundEnd=1) +
           vLineTo([8-tk,0], [8,10], tk, roundEnd=1) +
           vLineTo([4-hw/2,4], [hw,10], hw) +
           vLineTo([4-hw/2,4], [8,10], hw))
  elif(cpId == 78): ## N
    return(vLineTo([0,0], [tk,10], tk, roundEnd=1) +
           vLineTo([7-tk,0], [7,10], tk, roundEnd=2) +
           vDiagLineTo([0,10], [7,0], tk, reverse=True))
  elif(cpId == 79): ## O
    return(arc([4, 5], 8, 10, 0, 360, tk))
  elif(cpId == 80): ## P
    return(vLineTo([0,0], [tk,10], tk, roundEnd=1) +
           hLineTo([tk/2,5-tk/2],  [4,5+tk/2], tk) +
           hLineTo([tk/2,10-tk], [4,10], tk) +
           arc([4, 7.5-tk/4], 6, 5+tk/2, 270, 90, tk))
  elif(cpId == 81): ## Q
    return(arc([4, 5], 8, 10, 0, 360, tk) +
      vDiagLineTo([3,4], [8,0], tk, reverse=True, halfStart=True, roundEnd=3))
  elif(cpId == 82): ## R
    return(vLineTo([0,0], [tk,10], tk, roundEnd=1) +
           hLineTo([tk/2,5-tk/2], [4,5+tk/2], tk) +
           hLineTo([tk/2,10-tk], [4,10], tk) +
           arc([4, 7.5-tk/4], 6, 5+tk/2, 270, 90, tk) +
           vDiagLineTo([4, 5], [7,0], tk, reverse=True, roundEnd=2))
  elif(cpId == 83): ## S
    return(arc([3.5, 10-(5+tk/2)/2], 7, 5+tk/2, 0, 270, tk, roundEnd=1) +
           arc([3.5,    (5+tk/2)/2], 7, 5+tk/2, 180, 90, tk, roundEnd=1))
  elif(cpId == 84): ## T
    return(vLineTo([4-tk/2,0], [4+tk/2,10-tk], tk, roundEnd=1) +
           hLineTo([0,10-tk], [8,10], tk, roundEnd=3))
  elif(cpId == 85): ## U
    return(vLineTo([0,3.5], [tk,10], tk, roundEnd=2) +
           arc([3.5, 3.5], 7, 7, 180, 0, tk) +
           vLineTo([7-tk,3.5], [7,10], tk, roundEnd=2))
  elif(cpId == 86): ## V
    return(vDiagLineTo([0,10], [3.5,0] , tk, reverse=True, halfEnd=True, roundEnd=1) +
           vDiagLineTo([3.5,0], [7,10], tk, halfStart=True, roundEnd=2))
  elif(cpId == 87): ## W
    hw = getHWidth(2.5, 8, tk)
    hw2 = getHWidth(2.5-hw, 14, tk)
    return(vDiagLineTo([0,10], [2.5,0], tk, reverse=True, roundEnd=1) +
      vDiagLineTo([5.5,0], [8,10], tk, roundEnd=2) +
      vLineTo([2.5-hw,0], [4+hw2/2,7], hw2) +
      vLineTo([5.5,0], [4+hw2/2,7], hw2))
  elif(cpId == 88): ## X
    return(vDiagLineTo([0,0], [7,10], tk, roundEnd = 3) +
           vDiagLineTo([0,10], [7,0], tk, reverse=True, roundEnd = 3))
  elif(cpId == 89): ## Y
    hw = getHWidth(7, 10, tk)
    return(vDiagLineTo([0,10], [3.5,5] , tk, reverse=True, halfEnd=True, roundEnd=1) +
           vDiagLineTo([3.5,5], [7,10], tk, halfStart=True, roundEnd=2) +
           vLineTo([3.5-hw/2,0], [3.5+hw/2,5], hw, roundEnd=1))
  elif(cpId == 90): ## Z
    return(vDiagLineTo([0,tk], [7,10-tk], tk) +
           hLineTo([0,10-tk], [7,10], tk, roundEnd=1) +
           hLineTo([0,0], [7,tk], tk, roundEnd=2))
  elif(cpId == 91): ## [
    return(hLineTo([tk/2,0], [3,tk], tk, roundEnd=2) +
      hLineTo([tk/2,10-tk], [3,10], tk, roundEnd=2) +
      vLineTo([0,0], [tk,10], tk))
  elif(cpId == 92): ## \
    return(vDiagLineTo([0,10], [6.5, 0], tk, reverse=True, roundEnd=3))
  elif(cpId == 93): ## ]
    return(hLineTo([0,0], [3-tk/2,tk], tk, roundEnd=1) +
      hLineTo([0,10-tk], [3-tk/2,10], tk, roundEnd=1) +
      vLineTo([3-tk,0], [3,10], tk))
  elif(cpId == 94): ## ^
    return(vDiagLineTo([0, 6], [3,10], tk, halfEnd=True, roundEnd=1) +
      vDiagLineTo([3,10], [6,6], tk, reverse=True, halfStart = True, roundEnd=2))
  elif(cpId == 95): ## _
    return(hLineTo([0,0], [7,tk], tk, roundEnd=3))
  elif(cpId == 96): ## `
    return(vLineTo([2,6], [1, 9], tk/2) +
      vLineTo([2,6], [1+tk/2, 9], tk/2))
  elif(cpId == 97): ## a
    return(vLineTo([5.5-tk,2], [5.5,3], tk) +
      arc([2.5, 3], 6, 6, 0, 135, tk, roundEnd=2) +
      arc([2.75, 2], 5.5, 4, 0, 360, tk) +
      arc([6.5, 2], 2+2*tk, 4, 180, 270, tk, roundEnd=2))
  elif(cpId == 98): ## b
    return(vLineTo([0,3], [tk,10], tk, roundEnd=2) +
      arc([3, 3], 6, 6, 0, 360, tk))
  elif(cpId == 99): ## c
    return(arc([3, 3], 6, 6, 30, 330, tk, roundEnd=3))
  elif(cpId == 100): ## d
    return(vLineTo([6-tk,2], [6,10], tk, roundEnd=2) +
      arc([3, 3], 6, 6, 0, 360, tk) +
      arc([7, 2], 2+2*tk, 4, 180, 270, tk, roundEnd=2))
  elif(cpId == 101): ## e
    return(hLineTo([tk/2,3-tk/2], [6-tk/2,3+tk/2], tk) +
      vLineTo([6-tk,3-tk/2], [6,3], tk) +
      arc([3, 3], 6, 6, 0, 180, tk) +
      arc([3, 3], 6, 6, 180, 315, tk, roundEnd=2))
  elif(cpId == 102): ## f
    return(vLineTo([0,0], [tk,7], tk, roundEnd=1) +
      hLineTo([tk/2,6-tk], [3,6], tk, roundEnd=2) +
      arc([3, 7], 6, 6, 60, 180, tk, roundEnd=1))
  elif(cpId == 103): ## g
    return(vLineTo([6-tk,0], [6,3], tk) +
      arc([3, 0], 6, 6, 200, 360, tk, roundEnd=1) +
      arc([3, 3], 6, 6, 0, 360, tk))
  elif(cpId == 104): ## h
    return(vLineTo([0,0], [tk,10], tk, roundEnd=3) +
      vLineTo([6-tk,0], [6,3], tk, roundEnd=1) +
      arc([3, 3], 6, 6, 0, 180, tk))
  elif(cpId == 105): ## i
    return(vLineTo([1-tk/2,0], [1+tk/2,6], tk, roundEnd = 3) +
      arc([1, 7.5], tk*1.5, tk*1.5, 0, 360, tk))
  elif(cpId == 106): ## j
    return(vLineTo([4-tk,0], [4,6], tk, roundEnd=2) +
      arc([4-tk/2, 7.5], tk*1.5, tk*1.5, 0, 360, tk) +
      arc([2, 0], 4, 4, 180, 360, tk, roundEnd=1))
  elif(cpId == 107): ## k
    return(vLineTo([0,0], [tk,10], tk, roundEnd=3) +
      vDiagLineTo([0,3], [6,6], tk, roundEnd=2) +
      vDiagLineTo([0,3], [6,0], tk, reverse=True, roundEnd=2))
  elif(cpId == 108): ## l
    return(vLineTo([0,2], [tk,10], tk, roundEnd=2) +
      arc([2, 2], 4, 4, 180, 300, tk, roundEnd=2))
  elif(cpId == 109): ## m
    return(vLineTo([0,0], [tk,6], tk, roundEnd=3) +
      vLineTo([4-tk/2,0], [4+tk/2,6-(4+tk/2)/2], tk, roundEnd=1) +
      vLineTo([8-tk,0], [8,6-(4+tk/2)/2], tk, roundEnd=1) +
      arc([(4+tk/2)/2, 6-(4+tk/2)/2], (4+tk/2), (4+tk/2), 0, 180, tk) +
      arc([8-(4+tk/2)/2, 6-(4+tk/2)/2], (4+tk/2), (4+tk/2), 0, 180, tk))
  elif(cpId == 110): ## n
    return(vLineTo([0,0], [tk,6], tk, roundEnd=3) +
      vLineTo([6-tk,0], [6,3], tk, roundEnd=1) +
      arc([3, 3], 6, 6, 0, 180, tk))
  elif(cpId == 111): ## o
    return(arc([3, 3], 6, 6, 0, 360, tk))
  elif(cpId == 112): ## p
    return(vLineTo([0,-3], [tk,6], tk, roundEnd=3) +
      arc([3, 3], 6, 6, 0, 360, tk))
  elif(cpId == 113): ## q
    return(vLineTo([6-tk,-3], [6,6], tk, roundEnd=2) +
      vDiagLineTo([6-tk,-3], [6.5+tk,-1], tk, roundEnd=2) +
      arc([3, 3], 6, 6, 0, 360, tk))
  elif(cpId == 114): ## r
    return(vLineTo([0,0], [tk,6], tk, roundEnd=3) +
      arc([3, 3], 6, 6, 60, 180, tk, roundEnd=1))
  elif(cpId == 115): ## s
    return(arc([2.5, 6-(3+tk/2)/2], 5, 3+tk/2, 0, 270, tk, roundEnd=1) +
      arc([2.5,   (3+tk/2)/2], 5, 3+tk/2, 180, 90, tk, roundEnd=1))
  elif(cpId == 116): ## t
    return(vLineTo([0,2], [tk,10], tk, roundEnd=2) +
      hLineTo([tk,6-tk], [3,6], tk, roundEnd=2) +
      arc([2, 2], 4, 4, 180, 300, tk, roundEnd=2))
  elif(cpId == 117): ## u
    return(vLineTo([0,3], [tk,6], tk, roundEnd=2) +
      vLineTo([6-tk,2], [6,6], tk, roundEnd=2) +
      arc([3, 3], 6, 6, 180, 360, tk) +
      arc([7, 2], 2+2*tk, 4, 180, 270, tk, roundEnd=2))
  elif(cpId == 118): ## v
    return(vDiagLineTo([3,0], [6,6], tk, halfStart=True, roundEnd=2) +
      vDiagLineTo([0, 6], [3,0], tk, reverse=True, halfEnd=True, roundEnd=1))
  elif(cpId == 119): ## w
    hw = getHWidth(2.5, 6, tk)
    hw2 = getHWidth(2.5-hw, 8, tk)
    return(vDiagLineTo([0,6], [2.5,0], tk, reverse=True, roundEnd=1) +
      vDiagLineTo([5.5,0], [8,6], tk, roundEnd=2) +
      vLineTo([2.5-hw,0], [4+hw2/2,4], hw2) +
      vLineTo([5.5,0], [4+hw2/2,4], hw2))
  elif(cpId == 120): ## x
    return(vDiagLineTo([0,6], [6,0], tk, reverse=True, roundEnd=3) +
      vDiagLineTo([0,0], [6,6], tk, roundEnd=3))
  elif(cpId == 121): ## y
    return(vLineTo([0,3], [tk,6], tk, roundEnd=2) +
      vLineTo([6-tk,0], [6,6], tk, roundEnd=2) +
      arc([3, 0], 6, 6, 210, 360, tk, roundEnd=1) +
      arc([3, 3], 6, 6, 180, 360, tk))
  elif(cpId == 122): ## z
    return(vDiagLineTo([0,tk], [5,6-tk], tk) +
      hLineTo([0,6-tk], [5,6], tk, roundEnd=1) +
      hLineTo([0,0], [5,tk], tk, roundEnd=2))
  elif(cpId == 123): ## {
    return(arc([3, 7.5-tk/4], 3+tk, 5+tk/2, 90, 180, tk, roundEnd=1) +
      arc([0, 7.5-tk/4], 3+tk, 5+tk/2, 270, 0, tk, roundEnd=1) +
      arc([0, 2.5+tk/4], 3+tk, 5+tk/2, 0, 90, tk, roundEnd=2) +
      arc([3, 2.5+tk/4], 3+tk, 5+tk/2, 180, 270, tk, roundEnd=2))
  elif(cpId == 124): ## |
    return(vLineTo([1-tk/2,-1], [1+tk/2, 5-tk/2], tk, roundEnd=3) +
      vLineTo([1-tk/2,5+tk/2], [1+tk/2, 9], tk, roundEnd=3))
  elif(cpId == 125): ## }
    return(arc([0, 7.5-tk/4], 3+tk, 5+tk/2, 0, 90, tk, roundEnd=2) +
      arc([3, 7.5-tk/4], 3+tk, 5+tk/2, 180, 270, tk, roundEnd=2) +
      arc([3, 2.5+tk/4], 3+tk, 5+tk/2, 90, 180, tk, roundEnd=1) +
      arc([0, 2.5+tk/4], 3+tk, 5+tk/2, 270, 0, tk, roundEnd=1))
  elif(cpId == 126): ## ~
    return(arc([6.5 - (6.5+tk)/4, 7], (6.5+tk)/2, 4, 180, 360, tk, roundEnd=2) +
           arc([(6.5+tk)/4, 7], (6.5+tk)/2, 4, 0, 180, tk, roundEnd=2))
  elif(cpId == 196): ## A with umlaut
    return(arc([3-tk/2, 10-tk/2], tk, tk, 0, 360, tk) +
      arc([4+tk/2, 10-tk/2], tk, tk, 0, 360, tk) +
      vDiagLineTo([0,0], [3.5,8], tk, halfEnd=True, roundEnd=1) +
      vDiagLineTo([3.5,8], [7, 0], tk, reverse=True, halfStart=True, roundEnd=2) +
      hLineTo([(4-tk)*(2.5+tk/2)/10+tk/2,2.5],
      [7-((4-tk)*(2.5+tk/2)/10+tk/2),2.5+tk], tk))
  elif(cpId == 203): ## E with umlaut
    return(arc([2.5-tk/2, 10-tk/2], tk, tk, 0, 360, tk) +
      arc([3.5+tk/2, 10-tk/2], tk, tk, 0, 360, tk) +
      vLineTo([0,0], [tk,8], tk) +
      hLineTo([tk/2,0], [6,tk], tk, roundEnd=2) +
      hLineTo([tk/2,4-tk/2],  [4.75,4+tk/2], tk, roundEnd=2) +
      hLineTo([tk/2,8-tk], [6,8], tk, roundEnd=2))
  elif(cpId == 207): ## I with umlaut
    return(arc([1-tk/2, 10-tk/2], tk, tk, 0, 360, tk) +
      arc([2+tk/2, 10-tk/2], tk, tk, 0, 360, tk) +
      vLineTo([1.5-tk/2,0], [1.5+tk/2,8], tk, roundEnd=3))
  elif(cpId == 214): ## O with umlaut
    return(arc([3-tk/2, 10-tk/2], tk, tk, 0, 360, tk) +
      arc([4+tk/2, 10-tk/2], tk, tk, 0, 360, tk) +
      arc([3.5, 4], 7, 8, 0, 360, tk))
  elif(cpId == 220): ## U with umlaut
    return(arc([3-tk/2, 10-tk/2], tk, tk, 0, 360, tk) +
      arc([4+tk/2, 10-tk/2], tk, tk, 0, 360, tk) +
      vLineTo([0,3.5], [tk,8], tk, roundEnd=2) +
      arc([3.5, 3.5], 7, 7, 180, 0, tk) +
      vLineTo([7-tk,3.5], [7,8], tk, roundEnd=2))
  elif(cpId == 223): ## Sz
    return(vLineTo([0,0], [tk,8-tk], tk, roundEnd=1) +
      arc([2.25+tk/2, 8-tk], 4.5+tk, 4+2*tk, 0, 180, tk) +
      arc([4.5, 8-(4+tk/2)/2], 5, 4+tk/2, 90, 270, tk) +
      arc([4.5,    (4+tk/2)/2], 5, 4+tk/2, 210, 90, tk, roundEnd=1))
  elif(cpId == 228): ## a with umlaut
    return(arc([2.25-tk/2, 7.5], tk, tk, 0, 360, tk) +
           arc([3.25+tk/2, 7.5], tk, tk, 0, 360, tk) +
           vLineTo([5.5-tk,2], [5.5,3], tk) +
           arc([2.5, 3], 6, 6, 0, 135, tk, roundEnd=2) +
           arc([2.75, 2], 5.5, 4, 0, 360, tk) +
           arc([6.5, 2], 2+2*tk, 4, 180, 270, tk, roundEnd=2))
  elif(cpId == 235): ## e with umlaut
    return(arc([2.5-tk/2, 7.5], tk, tk, 0, 360, tk) +
      arc([3.5+tk/2, 7.5], tk, tk, 0, 360, tk) +
      hLineTo([tk/2,3-tk/2], [6-tk/2,3+tk/2], tk) +
      vLineTo([6-tk,3-tk/2], [6,3], tk) +
      arc([3, 3], 6, 6, 0, 180, tk) +
      arc([3, 3], 6, 6, 180, 315, tk, roundEnd=2))
  elif(cpId == 239): ## i with umlaut
    return(arc([1-tk/2, 7.5], tk, tk, 0, 360, tk) +
      arc([2+tk/2, 7.5], tk, tk, 0, 360, tk) +
      vLineTo([1.5-tk/2,0], [1.5+tk/2,6], tk, roundEnd=3))
  elif(cpId == 246): ## o with umlaut
    return(arc([2.5-tk/2, 7.5], tk, tk, 0, 360, tk) +
      arc([3.5+tk/2, 7.5], tk, tk, 0, 360, tk) +
      arc([3, 3], 6, 6, 0, 360, tk))
  elif(cpId == 252): ## u with umlaut
    return(arc([2.5-tk/2, 7.5], tk, tk, 0, 360, tk) +
      arc([3.5+tk/2, 7.5], tk, tk, 0, 360, tk) +
      vLineTo([0,3], [tk,6], tk, roundEnd=2) +
      vLineTo([6-tk,2], [6,6], tk, roundEnd=2) +
      arc([3, 3], 6, 6, 180, 360, tk) +
      arc([8-tk, 2], 4, 4, 180, 270, tk, roundEnd=2))
  elif(cpId == 256): ## A with macron
    return(hLineTo([1,10-tk], [6,10], tk, roundEnd=3) +
      vDiagLineTo([0,0], [3.5,8], tk, halfEnd=True, roundEnd=1) +
      vDiagLineTo([3.5,8], [7, 0], tk, reverse=True, halfStart=True, roundEnd=2) +
      hLineTo([(4-tk)*(2.5+tk/2)/10+tk/2,2.5],
      [7-((4-tk)*(2.5+tk/2)/10+tk/2),2.5+tk], tk))
  elif(cpId == 257): ## a with macron
    return(hLineTo([0.5,7], [5.5,7+tk], tk, roundEnd=3) +
      vLineTo([5.5-tk,2], [5.5,3], tk) +
      arc([2.5, 3], 6, 6, 0, 135, tk, roundEnd=2) +
      arc([2.75, 2], 5.5, 4, 0, 360, tk) +
      arc([6.5, 2], 2+2*tk, 4, 180, 270, tk, roundEnd=2))
  elif(cpId == 274): ## E with macron
    return(hLineTo([0.5,10-tk], [5.5,10], tk, roundEnd=3) +
      vLineTo([0,0], [tk,8], tk) +
      hLineTo([tk/2,0], [6,tk], tk, roundEnd=2) +
      hLineTo([tk/2,4-tk/2],  [4.75,4+tk/2], tk, roundEnd=2) +
      hLineTo([tk/2,8-tk], [6,8], tk, roundEnd=2))
  elif(cpId == 275): ## e with macron
    return(hLineTo([0.5,7.5-tk/2], [5.5,7.5+tk/2], tk, roundEnd=3) +
      hLineTo([tk/2,3-tk/2], [6-tk/2,3+tk/2], tk) +
      vLineTo([6-tk,3-tk/2], [6,3], tk) +
      arc([3, 3], 6, 6, 0, 180, tk) +
      arc([3, 3], 6, 6, 180, 315, tk, roundEnd=2))
  elif(cpId == 298): ## I with macron
    return(hLineTo([0,10-tk], [3,10], tk, roundEnd=3) +
      vLineTo([1.5-tk/2,0], [1.5+tk/2,8], tk, roundEnd=3))
  elif(cpId == 299): ## i with macron
    return(hLineTo([0,7.5-tk/2], [3,7.5+tk/2], tk, roundEnd=3) +
      vLineTo([1.5-tk/2,0], [1.5+tk/2,6], tk, roundEnd=3))
  elif(cpId == 332): ## O with macron
    return(hLineTo([1,10-tk], [6,10], tk, roundEnd=3) +
      arc([3.5, 4], 7, 8, 0, 360, tk))
  elif(cpId == 333): ## o with macron
    return(hLineTo([0.5,7.5-tk/2], [5.5,7.5+tk/2], tk, roundEnd=3) +
      arc([3, 3], 6, 6, 0, 360, tk))
  elif(cpId == 362): ## U with macron
    return(hLineTo([1,10-tk], [6,10], tk, roundEnd=3) +
      vLineTo([0,3.5], [tk,8], tk, roundEnd=2) +
      arc([3.5, 3.5], 7, 7, 180, 0, tk) +
      vLineTo([7-tk,3.5], [7,8], tk, roundEnd=2))
  elif(cpId == 363): ## u with macron
    return(hLineTo([0.5,7.5-tk/2], [5.5,7.5+tk/2], tk, roundEnd=3) +
      vLineTo([0,3], [tk,6], tk, roundEnd=2) +
      vLineTo([6-tk,2], [6,6], tk, roundEnd=2) +
      arc([3, 3], 6, 6, 180, 360, tk) +
      arc([8-tk, 2], 4, 4, 180, 270, tk, roundEnd=2))
  elif(cpId == 0x2013): ## <En-dash>
    return(hLineTo([0.5,3.5-tk/2], [5.5, 3.5+tk/2], tk, roundEnd=3))
  elif(cpId == 0x2014): ## <Em-dash>
    return(hLineTo([0.5,3.5-tk/2], [7.5, 3.5+tk/2], tk, roundEnd=3))
  elif(cpId == 0x2022): ## <Bullet>
    return(arc([1.5,3], 1 + tk*2, 1 + tk*2, 0, 360, 0.5 + tk*1))
  elif(cpId == 8216): ## ‘ [left single quote]
    return(arc([0.5,9.5-tk*2.25], 0.5 + tk*1.5, 0.5 + tk*1.5, 0, 360, 0.5 + tk*1.5) +
           arc([1.25+tk*0.5,9.5-tk*2.25], 2+tk * 2.5, 2+tk*2.5, 110, 180, 0.25+tk*0.25, roundEnd=1))
  elif(cpId == 8217): ## ’ [right single quote]
    return(arc([0.5,9.5-tk*0.75], 0.5 + tk*1.5, 0.5 + tk*1.5, 0, 360, tk*2) +
           arc([-0.25-tk*0.5,9.5-tk*0.75], 2+tk*2.5, 2+tk*2.5, 290, 0, 0.25+tk*0.25, roundEnd=1))
  elif(cpId == 8220): ## “ [left double quote]
    return(arc([2.75,9.5-tk*2.25], 0.5 + tk*1.5, 0.5 + tk*1.5, 0, 360, 0.5 + tk*1.5) + # right
           arc([3.5+tk*0.5,9.5-tk*2.25], 2+tk*2.5, 2+tk*2.5, 110, 180, 0.25+tk*0.25, roundEnd=1) +
           arc([2.75-0.5-tk*2,9.5-tk*2.25], 0.5 + tk*1.5, 0.5 + tk*1.5, 0, 360, 0.5 + tk*1.5) + # left
           arc([3.5-0.5-tk*2+tk*0.5,9.5-tk*2.25], 2+tk*2.5, 2+tk*2.5, 110, 180, 0.25+tk*0.25, roundEnd=1))
  elif(cpId == 8221): ## ” [right double quote]
    return(arc([1,9.5-tk*0.75], 0.5 + tk*1.5, 0.5 + tk*1.5, 0, 360, 0.5 + tk*1.5) +
           arc([0.25-tk*0.5,9.5-tk*0.75], 2+tk*2.5, 2+tk*2.5, 290, 0, 0.25 + tk*0.25, roundEnd=1) +
           arc([1+0.5+tk*2,9.5-tk*0.75], 0.5 + tk*1.5, 0.5 + tk*1.5, 0, 360, 0.5 + tk*1.5) +
           arc([0.25+0.5+tk*2-tk*0.5,9.5-tk*0.75], 2+tk*2.5, 2+tk*2.5, 290, 0, 0.25+tk*0.25, roundEnd=1))
  else:
    return(None) ## fall-through

## combines the [horizontal] shadows of two sub-regions
def combineShadows(s1, s2):
  shadowLength = len(s1[0])
  res = s1
  if((s1 is None) or (s2 is None)):
    return(None)
  if((len(s1) == 1) or (len(s2) == 1)):
    print(s1, "; ", s2)
    return(None)
  if((len(s1[1]) != len(s2[1]))):
    print(s1, "; ", s2)
    return(None)
  for si in range(shadowLength):
    res[0][si] = min(s1[0][si], s2[0][si])
    res[1][si] = max(s1[1][si], s2[1][si])
  return(res)

def makeKernShadow(cpId, tk = 1):
  global fontMode
  fontMode = "kern"
  res = fetchSplines(cpId, tk)
  if(res is not None):
    if(len(res) > 1):
      res = reduce(combineShadows, res)
    else:
      res = res[0]
  if(res is None):
    print("Bad shadow: ", cpId)
  return(res)

def guessKerning(cpId1, cpId2, tk = 1, gapWidth = 1):
  s1 = makeKernShadow(cpId1, tk = tk)
  s2 = makeKernShadow(cpId2, tk = tk)
  w1 = gW(cpId1) * 10
  w2 = gW(cpId2) * 10
  h1 = sum(map(lambda x: x < 81, s1[0]))
  h2 = sum(map(lambda x: x < 81, s2[0]))
  shadowLength = len(s1[0])
  minGap = (w1 + w2) + 2
  if((h1 < 35) or (h2 < 35)):
    # turn off kerning for small characters
    return(0)
  for si in range(shadowLength):
    minGap = min(minGap, (w1 - s1[1][si]) + (s2[0][si]))
  return(minGap)

def makeLetter(startId, cpId, tk = 1):
  global fontMode
  fontMode = "outline"
  res = ['StartChar: u%04x' % (cpId),
         "Encoding: %d %d %d" % (cpId, cpId, startId),
         "Width: %d" % ((gW(cpId) + gapSize) * (1576 / 10))]
  fs = fetchSplines(cpId, tk)
  if(fs[0] == ""):
    res += ["Flags: W"]
  else:
    res += ["Flags: H", "LayerCount: 2", "Fore", "SplineSet"]
    res += fetchSplines(cpId, tk)
    res += ["EndSplineSet"]
  if(cpId in kernWidths):
    kernStr = "Kerns2:"
    for cpId2 in kernWidths[cpId]:
      kernStr += " %d -%d \"KPs\"" % (
        orderLookup[cpId2], kernWidths[cpId][cpId2] )
    res += [kernStr]
  res += ["EndChar"]
  return(res)

def makeFontSet(fname, tk=1, weight="Regular"):
  global fontMode
  fontMode = "outline"
  numChars = 0
  for cpId in range(32,365):
    if(fetchSplines(cpId) is not None):
      numChars += 1
  header = """SplineFontDB: 3.2
FontName: Pointilised-%s-1.2.0
FullName: Pointilised %s
FamilyName: Pointilised
Weight: %s
Copyright: Copyright (c) %d, David Eccles
UComments: "%s: Created with makePointilisedGlyph.py (https://gitlab.com/gringer/pointilised)"
Version: 1.1.0
ItalicAngle: 0
UnderlinePosition: -100
UnderlineWidth: 50
Ascent: 1576
Descent: 472
InvalidEm: 0
LayerCount: 2
Layer: 0 0 "Back" 1
Layer: 1 0 "Fore" 0
Lookup: 258 0 0 "KP-sets" { "KPs" [307,30,0] } ['kern' ('DFLT' <'dflt' > 'latn' <'dflt' > ) ]
DEI: 91125
Encoding: UnicodeFull
Compacted: 1
UnicodeInterp: none
NameList: AGL For New Fonts
DisplaySize: -96
AntiAlias: 1
FitToEm: 0
WinInfo: 0 8 4
BeginChars: 1114112 %d""" % (weight, weight, weight, date.today().year, date.today(), numChars)
  footer = """
EndChars
EndSplineFont"""
  with open(fname, 'w') as f:
    f.write(header)
    for cpId in range(32,maxID):
      if(fetchSplines(cpId) is not None):
        f.write("\n\n" + "\n".join(makeLetter(orderLookup[cpId], cpId, tk=tk)))
    f.write(footer)

print("Creating kerning shadows based on Regular font")
kernShadows = dict()
orderLookup = dict()
with open("kernShadows.csv", 'w') as f:
  f.write("id,name,width,leftShadow,rightShadow\n")
  letterId = 0
  for cpId in range(32, maxID):
    if(fetchSplines(cpId) is None):
      continue
    orderLookup[cpId] = letterId
    letterId += 1
    if((cpId >= 65 and cpId <= 90) or
       (cpId >= 97 and cpId <= 122) or
       (cpId >= 196) or
       (cpId == 47) or (cpId == 92)):
      ks = makeKernShadow(cpId)
      if(ks is not None):
        kernShadows[cpId] = ks
        f.write("%d,%s,%d,%s,%s\n" %
                (cpId, unicodedata.name(chr(cpId)), gW(cpId),
                 ";".join(map(str,ks[0])),";".join(map(str,ks[1]))))

kernWidths = defaultdict(dict)

if(doKerning):
  print("Guessing kerning")
  for cpId1 in range(32, maxID):
    if(not cpId1 in kernShadows):
      continue
    for cpId2 in range(32, maxID):
      if(not cpId2 in kernShadows):
        continue
      gk = guessKerning(cpId1, cpId2)
      if(gk > (gapSize * 10)):
        gkb = (gk - gapSize * 10) # base glyph kern
        gks = int((gkb * (1576 / 1000)) + 0.5) * 10 # scaled glyph kern
        kernWidths[cpId1][cpId2] = gks
        #sys.stdout.write("%d %s, %d %s, %d, %d\n" %
        #( cpId1, unicodedata.name(chr(cpId1)),
        #  cpId2, unicodedata.name(chr(cpId2)), gkb, gks ) )


print("Making Light font set")
makeFontSet("pointilised_0.5.sfd", tk=16/32, weight="Light") # 0.5 px
print("Making Regular font set")
makeFontSet("pointilised_1.0.sfd", tk=32/32, weight="Regular") # 1 px (hopefully)
print("Making Bold font set")
makeFontSet("pointilised_1.5.sfd", tk=48/32, weight="Bold") # 1.5 px
