// Parametric letter module
// David Eccles (gringer) <bioinformatics@gringene.org>

$fn=30;

// get [letter] Width
function gW(val) =
  (val < 65) ? // pre-letter symbols
  ((val == 39) || // '
   (val == 44) || // ,
   (val == 46)) ? 1 : // .
  ((val == 33) || // !
   (val == 58) || // :
   (val == 59)) ? 2 : // ;
  ((val == 32) || // [space]
   (val == 34) || // "
   (val == 40) || // (
   (val == 41)) ? 3.5 : // )
  (val == 63) ? 5 : // ?
  ((val == 38) || // &
   (val == 47)) ? 6.5 : // /
  (val == 64) ? 7 : // @
  6.5 : 
  (val < 97) ? // upper-case letters
  ((val == 73)) ? 2 : // I
  ((val == 91) || // [
   (val == 93) || // ]
   (val == 96)) ? 3 : // `
  ((val == 69) || // E
   (val == 70) || // F
   (val == 74) || // J
   (val == 76) || // L
   (val == 94)) ? 6 : // ^
  ((val == 92)) ? 6.5 : // \ 
  ((val == 65) || // A
   (val == 66) || // B
   (val == 67) || // C
   (val == 68) || // D
   (val == 71) || // G
   (val == 72) || // H
   (val == 75) || // K
   (val == 78) || // N
   (val == 80) || // P
   (val == 82) || // R
   (val == 83) || // S
   (val == 85) || // U
   (val == 86) || // V
   (val == 88) || // X
   (val == 89) || // Y
   (val == 90) || // Z
   (val == 95)) ? 7 : // _
  ((val == 84) || // T
   (val == 87)) ? 8 : // W
  8 :
  (val < 127) ? // lower-case letters
  ((val == 105) || // i
   (val == 124)) ? 2 : // |
  ((val == 108) || // l
   (val == 116) || // t
   (val == 123) || // {
   (val == 125)) ? 3 : // }
  ((val == 106)) ? 4 : // j
  ((val == 102) || // f
   (val == 114)) ? 4.5 : // r
   ((val == 107) || // k
   (val == 115)) ? 5 : // s
  ((val == 100) || // d
   (val == 126) || // ~
   (val == 117)) ? 6.5 : // u
  ((val == 109) || // m
   (val == 119)) ? 8 : // w
  6 :
  ((val == 8216) || // ‘ [left single quote]
   (val == 8217) || // ’ [right single quote]
   (val == 9999)) ? 1 :
  ((val == 207) || // Ï
   (val == 298) || // Ī
   (val == 239) || // ï
   (val == 299)) ? 3 : // ī
  ((val == 203) || // Ë
   (val == 274) || // Ē
   (val == 235) || // ë
   (val == 275) || // ē
   (val == 228) || // ä
   (val == 257)) ? 6 : // ā
  ((val == 8220) || // “ [left double quote]
   (val == 8221) || // ” [right double quote]
   (val == 9999)) ? 3.5 :
  ((val == 246) || // ö
   (val == 333) || // ō 
   (val == 252) || // ü
   (val == 363)) ? 6.5 : // ū
  ((val == 196) || // Ä
   (val == 256) || // Ā
   (val == 214) || // Ö
   (val == 332) ||  // Ō
   (val == 220) || // Ü
   (val == 362) || // Ū
   (val == 223)) ? 7 : // ß
   6.5;

function sumLwds(A,start,end,value=0) =
  (start > end) ? 0 : (start == end ? value + gW(A[start]) : 
    sumLwds(A,start+1,end,value+gW(A[start])));

// Magic diagonal width calculation, provided by Twitter/@LBelenky
// https://twitter.com/LBelenky/status/1428887569660628993
// w, h, t are width/height/thickness respectively
// derivation / proof by @spacemagick@mastodon.social
// based on similar triangles:
// A) height t, width sqrt(x^2 - t^2) [small triangle in corner]
// B) height h, width (w-x)  [large triangle, from mid point to mid point]
// [https://genomic.social/@spacemagick@mastodon.social/112819018138838912]
function getHWidth(w, h, t) =
  ( t * (h * sqrt(h^2 - t^2 + w^2) - w * t) /
    (h^2 - t^2) );

function getRoundedAngle(w, h, th, half = false) =
  // Determines the angle for the rounded bottom of a vDiagLine
  let(Wh = getHWidth(w * ((half) ? 2 : 1), h * ((half) ? 2 : 1), th) / ((half) ? 2 : 1))
  (Wh == th) ? (th / 2) :
  (atan((w-Wh) / h));

function getRoundedDist(w, h, th, half = false) =
  // Determines the horizontal displacement for the rounded bottom of a vDiagLine
  (w == 0) ? (th/2) :
  let(Wh = getHWidth(w * ((half) ? 2 : 1), h * ((half) ? 2 : 1), th) / ((half) ? 2 : 1),
      theta = atan((w-Wh) / h))
  (th/2 * (cos(theta) + tan(theta)*sin(theta) + tan(theta)));

function makeSegment(start, end, addEnd=false) =
  let(segLen = sqrt((end[0]-start[0])^2 + (end[1]-start[1])^2),
      steps = max(ceil(segLen / (10 / ($fn))), 1),
      resPoints = [for (i = [0 : (steps-1)])
                   start + (end-start) * (i / steps) ])
      addEnd ? concat(resPoints, [end]) : resPoints;

function arbLineTo(start, end, sAng, eAng, th) =
  let(lineAng = atan2(end[1]-start[1], end[0]-start[0]),
      ofsAng = lineAng + 90)
  concat(
    makeSegment([start[0], start[1]], [end[0]-th, end[1]]),
    makeSegment([end[0]-th, end[1]],  [end[0], end[1]]),
    makeSegment([end[0], end[1]], [start[0]+th, start[1]]),
    makeSegment([start[0]+th, start[1]], [start[0], start[1]]));

function arcEnd(arcCentre, diameter, angFrom, angTo, dir) = 
  let(thTo= ((dir > 0) && (angTo < angFrom)) ? (angTo + 360) : angTo,
      thFrom = ((dir < 0) && (angFrom < angTo)) ? (angFrom + 360) : angFrom,
      steps=ceil(abs(thTo - thFrom) / (360 / ($fn*2))),
      incAng=(thTo - thFrom) / steps,
      cr=diameter/2)
   [ for(theta = [thFrom : incAng : thTo])
        [(cr) * cos(theta) + arcCentre[0],
         (cr) * sin(theta) + arcCentre[1]] ];

function arc(arcCentre, w, h, thFrom, thToMod, thickness, roundEnd = 0) =
  let(thTo=(thToMod < thFrom) ? (thToMod + 360) : thToMod,
      steps=ceil(abs(thTo-thFrom) / (360 / ($fn*2))),
      incAng=(thTo-thFrom)/steps,
      th=thickness/2,
      wr=w/2,
      hr=h/2)
  (((thickness >= w/2) || (thickness >= h/2)) && (thTo-thFrom > 359)) ?
   [ for(theta = [thTo : -incAng : thFrom]) // closed circle
        [(wr) * cos(theta) + arcCentre[0],
         (hr) * sin(theta) + arcCentre[1]] ] :
    concat(
      [ for(theta = [thTo : -incAng : thFrom]) // outer circle
        [(wr) * cos(theta) + arcCentre[0],
         (hr) * sin(theta) + arcCentre[1]] ],
      (roundEnd % 2 == 1) ? 
        let(ec = [(wr - thickness/2) * cos(thFrom),
                  (hr - thickness/2) * sin(thFrom)] + arcCentre)
        [ for(thr = [(thFrom) : -10 : (thFrom-180)])
          ([th * cos(thr),th * sin(thr)] + ec) ] : [],
      [ for(theta = [thFrom : incAng : thTo]) // inner circle
        [(wr-thickness) * cos(theta) + arcCentre[0],
         (hr-thickness) * sin(theta) + arcCentre[1]] ],
      (roundEnd - 2 >= 0) ? 
        let(ec = [(wr - thickness/2) * cos(thTo),
                  (hr - thickness/2) * sin(thTo)] + arcCentre)
        [ for(thr = [(thTo+180) : -10 : (thTo)])
          ([th * cos(thr),th * sin(thr)] + ec) ] : [],
      []);

// A vertical line with horizontally-oriented end butts
// the [start] and [end] points define the bottom left and top right
// corners of the line respectively, including thickness. If the ends
// are rounded, then the line length is reduce to accommodate the ends.
// note: this doesn't modify thickness due to diagonal angle;
//       for that, see vDiagLineTo
function vLineTo(start, end, th, roundEnd = 0) =
  let(rRad = abs(end[0]-start[0])/2,
      lowerAdj = (roundEnd % 2 == 1) ? rRad : 0,
      upperAdj = (roundEnd - 2 >= 0) ? rRad : 0,
      lowerMid = [(start[0] + end[0]) / 2, start[1] + lowerAdj],
      upperMid = [(start[0] + end[0]) / 2, end[1] - upperAdj],
      p1 = [start[0], start[1] + lowerAdj],
      p2 = [start[0], end[1] - upperAdj],
      p3 = [end[0], end[1] - upperAdj],
      p4 = [end[0], start[1] + lowerAdj]
      )
  concat(
         makeSegment(p1, p2),
         ((roundEnd - 2) >= 0) ?
            arcEnd(upperMid, rRad*2, 180, 0, -1) : makeSegment(p2, p3),
         makeSegment(p3, p4),
         ((roundEnd % 2) == 1) ?
            arcEnd(lowerMid, rRad*2, 0, 180, -1) : makeSegment(p4, p1));

// a horizontal line with vertically-oriented end butts
// the [start] and [end] points define the bottom left and top right
// corners of the line respectively, including thickness. If the ends
// are rounded, then the line length is reduce to accommodate the ends.
// note: this doesn't modify thickness due to diagonal angle;
//       for that, see vDiagLineTo [horizontally-oriented end butts]
function hLineTo(start, end, th, roundEnd = 0) =
  let(rRad = abs(end[1]-start[1])/2,
      leftAdj = (roundEnd % 2 == 1) ? rRad : 0,
      rightAdj = (roundEnd - 2 >= 0) ? rRad : 0,
      leftMid = [start[0] + leftAdj, (start[1] + end[1]) / 2],
      rightMid = [end[0] - rightAdj, (start[1] + end[1]) / 2],
      p1 = [start[0] + leftAdj, start[1]],
      p2 = [start[0] + leftAdj, end[1]],
      p3 = [end[0] - rightAdj, end[1]],
      p4 = [end[0] - rightAdj, start[1]]
      )
  concat(
           ((roundEnd % 2) == 1) ?
              arcEnd(leftMid, rRad*2, 270, 90, -1) : makeSegment(p1, p2),
           makeSegment(p2, p3),
           ((roundEnd - 2) >= 0) ?
              arcEnd(rightMid, rRad*2, 90, 270, -1) : makeSegment(p3, p4),
           makeSegment(p4, p1));

function rev(input) =
  [for(i = [0:(len(input) - 1)]) input[(len(input)-1) - i]];

// a diagonal line with horizontally-oriented end butts
// the [start] and [end] points define the left and right edges of the line
// respectively, including thickness. The line is created so that the thickness
// of the line perpendicular to the line direction is the same as the specified
// thickness.
// The halfStart and halfEnd parameters indicate whether the anchor point
// should be at the mid-point of the line, instead of the extremity.
function vDiagLineTo(start, end, th, reverse=false,
                     halfStart=false, halfEnd=false, roundEnd = 0) =
  let(w = abs(end[0] - start[0]),
      h = abs(end[1] - start[1]),
      x = (!halfStart && !halfEnd) ? getHWidth(w, h, th) : // no half ends
          ( halfStart &&  halfEnd) ? (th / sin(atan(h / w))) : // two half ends
          getHWidth(w*2, h*2, th)) // one half end
  let(eofs = (halfEnd) ? (x / 2) : 0, // end offset
      sofs = (halfStart) ? (x / 2) : 0, // start offset
      xMul = sign(end[0] - start[0]),
      yMul = sign(end[1] - start[1]),
      p1t = [start[0] -     sofs, start[1]], // left vertex
      p2t = [start[0] + x - sofs, start[1]], // left vertex + offset
      p3t = [  end[0] +     eofs,   end[1]], // right vertex
      p4t = [  end[0] - x + eofs,   end[1]], // right vertex - offset
      p1n = (yMul > 0) ? p4t : p1t, // normalise to clockwise; p1n = Top Left
      p2n = (yMul > 0) ? p3t : p2t, // normalise to clockwise; p2n = Top Right
      p3n = (yMul > 0) ? p2t : p3t, // normalise to clockwise; p3n = Bottom Right
      p4n = (yMul > 0) ? p1t : p4t, // normalise to clockwise; p4n = Bottom Left
      roundBottom = (yMul > 0) ? (roundEnd % 2 == 1) : (roundEnd - 2 >= 0),
      roundTop = (yMul > 0) ? (roundEnd - 2 >= 0) : (roundEnd % 2 == 1)
      )
  (roundEnd == 0) ?
    concat(makeSegment(p1n, p2n), makeSegment(p2n, p3n),
           makeSegment(p3n, p4n), makeSegment(p4n, p1n)) :
    let(pTmid = [(p1n[0] + p2n[0])/2, (p1n[1] + p2n[1])/2], // top mid point
        pBmid = [(p3n[0] + p4n[0])/2, (p3n[1] + p4n[1])/2], // bottom mid point
        dx = pTmid[0] - pBmid[0],
        dy = pTmid[1] - pBmid[1],
        theta = (dx == 0) ? 90 : atan2(dy, dx),
        pTheta = (theta + 90),
        xofs = (dx == 0) ? 0 : (th / (2 * tan(theta))),
        pT = [pTmid[0] - xofs, pTmid[1] - th/2],
        pB = [pBmid[0] + xofs, pBmid[1] + th/2],
        pDelta = [th/2 * cos(pTheta), th/2 * sin(pTheta)],
        p1s =    (roundTop) ? (pT - pDelta) : p2n, // rounded end points
        p2s =    (roundTop) ? (pT + pDelta) : p1n, // TODO: don't know why these
        p3s = (roundBottom) ? (pB + pDelta) : p4n, //       need to be flipped
        p4s = (roundBottom) ? (pB - pDelta) : p3n,
        ptc = arcEnd(pT, th, theta-90, theta+90, 1),
        pbc = arcEnd(pB, th, theta+90, theta-90, 1))
  rev(concat((roundTop) ? ptc : makeSegment(p1s, p2s), makeSegment(p2s, p3s),
             (roundBottom) ? pbc : makeSegment(p3s, p4s), makeSegment(p4s, p1s)));

// a diagonal line with vertically-oriented end butts
// the [start] and [end] points define the left and right edges of the line
// respectively, including thickness. The line is created so that the thickness
// of the line perpendicular to the line direction is the same as the specified
// thickness.
// The halfStart and halfEnd parameters indicate whether the anchor point
// should be at the mid-point of the line, instead of the extremity.
function hDiagLineTo(start, end, th, reverse=false,
                     halfStart=false, halfEnd=false, roundEnd = 0) =
  let(w = abs(end[0] - start[0]),
      h = abs(end[1] - start[1]),
      x = (!halfStart && !halfEnd) ? getHWidth(h, w, th) : // no half ends
          ( halfStart &&  halfEnd) ? (th / sin(atan(w / h))) : // two half ends
          getHWidth(h*2, w*2, th)) // one half end
  let(eofs = (halfEnd) ? (x / 2) : 0, // end offset
      sofs = (halfStart) ? (x / 2) : 0, // start offset
      xMul = sign(end[0] - start[0]),
      p1t = [start[0], start[1] -     sofs], // left bottom edge
      p2t = [start[0], start[1] + x - sofs], // left bottom edge + offset
      p3t = [  end[0],   end[1] +     eofs], // right top edge
      p4t = [  end[0],   end[1] - x + eofs] // right top edge - offset
      )
  concat(makeSegment(p1t, p2t), makeSegment(p2t, p3t),
        makeSegment(p3t, p4t), makeSegment(p4t, p1t));


function strLen(id, gap=0.5) =
    (len(id) == 0) ? 0 :
      let(cslWidths = cumSum([for (l = id) gW(l) + gap]))
      cslWidths[len(cslWidths) - 1];

function ltrPointsArr(id, tk=1.5, gap=0.5) =
    let(cslWidths = cumSum([for (l = id) gW(l) + gap]))
    flatten(
      [for(i = [0:(len(id) - 1)]) 
        let(lpp = (ltrPoints(id[i], tk=tk)))
        [ for(lp = lpp)
          [ for(p = lp) p + [cslWidths[i],0] ] ] ]);

function ltrPoints(id, tk=1.5) =
  (id == 32) ? // <space>
    [] :
  (id == 33) ? // !
    concat(
          [arc([1, 10-(tk+1)/2], tk*2, tk*2, 0, 180, tk)],
          [vDiagLineTo([1,2.5], [1, 10-(tk+1)/2], tk, halfStart=true)],
          [vDiagLineTo([1,2.5], [1+tk, 10-(tk+1)/2], tk, halfStart=true)],
          [arc([1, 1], tk*1.5, tk*1.5, 0, 360, tk)]) :
  (id == 8220) ? // “ [left double quotes]
    concat(
      [arc([2.5,10-tk*1.75], tk*1.5, tk*1.5, 0, 360, tk*1.5)],
      [arc([2.5+tk*1.25,10-tk*1.75], tk*4, tk*4, 110, 180, tk*0.5, roundEnd=1)],
      [arc([2.5-tk*2,10-tk*1.75], tk*1.5, tk*1.5, 0, 360, tk*1.5)],
      [arc([2.5-tk*2+tk*1.25,10-tk*1.75], tk*4, tk*4, 110, 180, tk*0.5, roundEnd=1)]) :
  (id == 8221) ? // ” [right double quotes]
    concat(
      [arc([1,10-tk/2], tk*1.5, tk*1.5, 0, 360, tk*1.5)],
      [arc([1-tk*1.25,10-tk/2], tk*4, tk*4, 290, 0, tk*0.5, roundEnd=1)],
      [arc([1+tk*2,10-tk/2], tk*1.5, tk*1.5, 0, 360, tk*1.5)],
      [arc([1+tk*2-tk*1.25,10-tk/2], tk*4, tk*4, 290, 0, tk*0.5, roundEnd=1)]) :
  (id == 34) ? // "
    concat(
           [vDiagLineTo([0.75-tk/4,7], [0.75, 10], tk/2)],
           [vDiagLineTo([0.75-tk/4,7], [0.75+tk/2, 10], tk/2)],
           [vDiagLineTo([2.75-tk/4,7], [2.75, 10], tk/2)],
           [vDiagLineTo([2.75-tk/4,7], [2.75+tk/2, 10], tk/2)]) :
  (id == 35) ? // #
    concat(
           [vDiagLineTo([0.5,1], [1.5+tk,9], tk, roundEnd=3)],
           [vDiagLineTo([5-tk,1], [6,9], tk, roundEnd=3)],
           [hLineTo([0,6.5-tk/2], [6.5,6.5+tk/2], tk, roundEnd=3)],
           [hLineTo([0,3.5-tk/2], [6.5,3.5+tk/2], tk, roundEnd=3)]) :
  (id == 36) ? // $
    concat(
      [vLineTo([3.5-tk/2,0], [3.5+tk/2,10], tk, roundEnd=3)],
      [arc([3.5, 9-(4+tk/2)/2], 6.5, 4+tk/2, 30, 270, tk, roundEnd=1)],
      [arc([3.5, 1+(4+tk/2)/2], 6.5, 4+tk/2, 210, 90, tk, roundEnd=1)]) :
  (id == 37) ? // %
    concat(
      [arc([1.5, 7.5], 3, 3, 0, 360, tk)],
      [arc([5, 2.5], 3, 3, 0, 360, tk)],
      [vDiagLineTo([1,1], [5.5,9], tk, roundEnd=3)]) :
  (id == 38) ? // & [tricky; needs work]
    concat(
      [arc([6.5, 6.5], 11, 11, 180, 270, tk, roundEnd=2)],
      [arc([2.75, 6.5], 3.5, 5, 0, 180, tk)],
      [arc([2, 6.5], 5, 3 + 2 * tk, 270, 360, tk)],
      [arc([2, 3], 4, 4, 90, 270, tk)],
      [arc([2, 6.5], 11, 11, 270, 330, tk, roundEnd=2)]) :
  (id == 8216) ? // ‘ [left single quote]
    concat(
      [arc([-0.5,10-tk*1.75], tk*1.5, tk*1.5, 0, 360, tk*1.5)],
      [arc([-0.5+tk*1.25,10-tk*1.75], tk*4, tk*4, 110, 180, tk*0.5, roundEnd=1)]) :
  (id == 8217) ? // ’ [right single quote]
    concat(
      [arc([1,10-tk/2], tk*1.5, tk*1.5, 0, 360, tk*1.5)],
      [arc([1-tk*1.25,10-tk/2], tk*4, tk*4, 290, 0, tk*0.5, roundEnd=1)]) :
  (id == 39) ? // ' [straight single quote]
    concat(
          [vLineTo([0.5-tk/4,7], [0.5, 10], tk/2)],
          [vLineTo([0.5-tk/4,7], [0.5+tk/2, 10], tk/2)]) :
  (id == 40) ? // (
    concat(
      [arc([3.25, 5], 6.5, 10, 90, 270, tk, roundEnd=3)]) :
  (id == 41) ? // )
    concat(
      [arc([0, 5], 6.5, 10, 270, 90, tk, roundEnd=3)]) :
  (id == 42) ? // *
    concat(
      [vDiagLineTo([0,7], [6.5,3], tk, roundEnd=3)],
      [vDiagLineTo([0,3], [6.5,7], tk, roundEnd=3)],
      [vLineTo([3.25-tk/2, 2], [3.25+tk/2, 8], tk, roundEnd=3)]) :
  (id == 43) ? // +
    concat(
      [vLineTo([3.25-tk/2,1.75], [3.25+tk/2, 8.25], tk, roundEnd=3)],
      [hLineTo([0,5-tk/2], [6.5, 5+tk/2], tk, roundEnd=3)]) :
  (id == 44) ? // ,
    concat(
      [arc([0.5,0.5], tk*1.5, tk*1.5, 0, 360, tk*1.5)],
      [arc([0.5-tk*1.25,0.5], tk*4, tk*4, 290, 0, tk*0.5, roundEnd=1)]) :
  (id == 45) ? // -
    concat(
      [hLineTo([0.5,5-tk/2], [6, 5+tk/2], tk, roundEnd=3)]) :
  (id == 46) ? // .
    concat(
          [arc([0.5,0.5], tk*1.5, tk*1.5, 0, 360, tk*1.5)]) :
  (id == 47) ? // /
    concat(
          [vDiagLineTo([0,0], [6.5, 10], tk, roundEnd=3)]) :
  (id == 48) ? // 0
    concat(
      [vLineTo([6.5-tk,3.5], [6.5,6.5], tk)],
      [vLineTo([0,3.5], [tk,6.5], tk)],
      [hDiagLineTo([tk,3.25], [6.5-tk,6.5], tk)],
      [arc([3.25, 6.5], 6.5, 7, 0, 180, tk)],
      [arc([3.25, 3.5], 6.5, 7, 180, 0, tk)]) :
  (id == 49) ? // 1
    concat(
      [vDiagLineTo([1.5-tk,7], [(6.5+tk)/2,10], tk, roundEnd=1)],
      [vLineTo([(6.5-tk)/2,0], [(6.5+tk)/2,10], tk)],
      [hLineTo([1.5-tk/2,0], [5+tk/2,tk], tk, roundEnd=3)]) :
  (id == 50) ? // 2
    let(rx=(13 - sqrt(2) * tk)/(4-2 * sqrt(2)),
        ry=((6.75) * sqrt(2)) / 2 + tk/2)
    concat(
      [hLineTo([tk/2,0], [6.5,tk], tk, roundEnd=2)],
      [arc([3.25, 6.75], 6.5, 6.5, 0, 180, tk, roundEnd=2)],
      [arc([6.5-rx, 6.75], 2*rx, 2 * ry, 315, 360, tk)],
      [arc([rx, 0], 2*rx, 2 * ry, 135, 180, tk)]) :
  (id == 51) ? // 3
    concat(
      [arc([3.25, 10-(10+tk)/4], 6.5, (10+tk)/2, 270, 180, tk, roundEnd=2)],
      [hLineTo([3,5-tk/2], [3.5,5+tk/2], tk)],
      [arc([3.25, (10+tk)/4], 6.5, (10+tk)/2, 180, 90, tk, roundEnd=1)],
      [arcEnd([3, 5], tk, 90, 270, 1)]) :
  (id == 52) ? // 4
    concat(
      [vLineTo([5-tk,0], [5,10], tk, roundEnd=1)],
      [hLineTo([0,3.5-tk/2], [6.5,3.5+tk/2], tk, roundEnd=2)],
      [vDiagLineTo([0,3.5+tk/2], [5,10], tk)]) :
  (id == 53) ? // 5
    concat(
      [vLineTo([0,6.5], [tk,10-tk], tk)],
      [arc([tk, 6.5], tk*2, tk*2, 180, 270, tk)],
      [hLineTo([tk,6.5-tk], [3.25,6.5], tk)],
      [hLineTo([0,10-tk], [6,10], tk, roundEnd=2)],
      [arc([3.25, 6.5/2], 6.5, 6.5, 180, 90, tk, roundEnd=1)]) :
  (id == 54) ? // 6
    concat(
      [arc([5, 3], 10, (10-3) * 2, 90, 180, tk, roundEnd=1)],
      [arc([3.25, 3], 6.5, 6, 0, 180, tk)],
      [arc([3.25, 3], 6.5, 6, 180, 360, tk)]) :
  (id == 55) ? // 7
    concat(
      [hLineTo([0,10-tk], [6.5,10], tk, roundEnd=1)],
      [vDiagLineTo([2,0], [6.5,10-tk], tk, roundEnd=1)]) :
  (id == 56) ? // 8
    concat(
      [arc([3.25, 10-(10+tk)/4], 6.5, (10+tk)/2, 0, 180, tk)],
      [arc([3.25, 10-(10+tk)/4], 6.5, (10+tk)/2, 180, 360, tk)],
      [arc([3.25, (10+tk)/4], 6.5, (10+tk)/2, 0, 180, tk)],
      [arc([3.25, (10+tk)/4], 6.5, (10+tk)/2, 180, 360, tk)]) :
  (id == 57) ? // 9
    concat(
      [vDiagLineTo([6.5-tk*2,0], [6.5,7], tk, roundEnd=1)],
      [arc([3.25, 7], 6.5, 6, 0, 180, tk)],
      [arc([3.25, 7], 6.5, 6, 180, 360, tk)]) :
  (id == 58) ? // :
    concat(
      [arc([1,3], tk*1.5, tk*1.5, 0, 360, tk)],
      [arc([1,7], tk*1.5, tk*1.5, 0, 360, tk)]) :
  (id == 59) ? // ;
    concat(
      [arc([1,7], tk*1.5, tk*1.5, 0, 360, tk)],
      [arc([1,3], tk*1.5, tk*1.5, 0, 360, tk*1.5)],
      [arc([1-tk*1.25,3], tk*4, tk*4, 290, 0, tk*0.5, roundEnd=1)]) :
  (id == 60) ? // <
    concat(
      [vDiagLineTo([0.5,5], [6,9], tk, roundEnd=2)],
      [vDiagLineTo([0.5,5], [6,1], tk, roundEnd=2)]) :
  (id == 61) ? // =
    concat(
      [hLineTo([0,6.5-tk/2], [6.5,6.5+tk/2], tk, roundEnd=3)],
      [hLineTo([0,3.5-tk/2], [6.5,3.5+tk/2], tk, roundEnd=3)]) :
  (id == 62) ? // >
    concat(
      [vDiagLineTo([0.5,9], [6,5], tk, roundEnd=1)],
      [vDiagLineTo([0.5,1], [6,5], tk, roundEnd=1)]) :
  (id == 63) ? // ?
    let(r=(-sqrt(2) * tk + tk + 5)/(4-2 * sqrt(2)),
        dy = (r-tk/2) * (2/sqrt(2)))
    concat(
      [arc([2.5, 7.5], 5, 5, 0, 180, tk, roundEnd=2)],
      [arc([5-r, 7.5], 2*r, 2*r, 315, 360, tk)],
      [arc([(2.5-tk/2)+r, 7.5-dy], 2*r, 2*r, 135, 180, tk, roundEnd=2)],
      [arc([2.5, 1], tk*1.5, tk*1.5, 0, 360, tk)]) :
  (id == 64) ? // @
    concat(
          [arc([3.5, 5], 3.5, 4.5, 0, 360, tk)],
          [arc([(12.25 - tk)/2, 5], (6.625 - (11.5 - tk)/2)*2, 5, 180, 360, tk)],
          [arc([3.5, 5], 7, 8, 0, 290, tk, roundEnd=2)]) :
  (id == 65) ? // A
    concat(
      [vDiagLineTo([0,0], [3.5,10], tk, halfEnd=true, roundEnd = 1)],
      [vDiagLineTo([3.5,10], [7, 0], tk, reverse=true,
                   halfStart=true, roundEnd = 2)],
      [hLineTo([(3.5-tk)*(2.5+tk/2)/10+tk/2,2.5],
               [7-((3.5-tk)*(2.5+tk/2)/10+tk/2),2.5+tk], tk)]) :
  (id == 66) ? // B
    concat(
      [vLineTo([0,0], [tk,10], tk)],
      [hLineTo([tk/2,0], [4,tk], tk)],
      [hLineTo([tk/2,4.5],  [4,5+tk/2], tk)],
      [hLineTo([tk/2,10-tk], [4,10], tk)],
      [arc([4, 7.5-tk/4], 6, 5+tk/2, 270, 90, tk)],
      [arc([4, 2.5+tk/4], 6, 5+tk/2, 270, 90, tk)]) :
  (id == 67) ? // C
    concat(
      [vLineTo([0,3.5], [tk,6.5], tk)],
      [arc([3.5, 6.5], 7, 7, 0, 180, tk, roundEnd=1)],
      [arc([3.5, 3.5], 7, 7, 180, 0, tk, roundEnd=2)]) :
  (id == 68) ? // D
    concat(
      [vLineTo([7-tk,3.5], [7,6.5], tk)],
      [vLineTo([0,0], [tk,10], tk)],
      [hLineTo([tk-0.01,10-tk], [3.5, 10], tk)],
      [hLineTo([tk-0.01,0], [3.5, tk], tk)],
      [arc([3.5, 6.5], 7, 7, 0, 90, tk)],
      [arc([3.5, 3.5], 7, 7, 270, 0, tk)]) :
  (id == 69) ? // E
    concat(
      [vLineTo([0,0], [tk,10], tk)],
      [hLineTo([tk/2,0], [6,tk], tk, roundEnd=2)],
      [hLineTo([tk/2,5-tk/2],  [4.75,5+tk/2], tk, roundEnd=2)],
      [hLineTo([tk/2,10-tk], [6,10], tk, roundEnd=2)]) :
  (id == 70) ? // F
    concat(
      [vLineTo([0,0], [tk,10], tk, roundEnd=1)],
      [hLineTo([tk/2,5-tk/2],  [4.75,5+tk/2], tk, roundEnd=2)],
      [hLineTo([tk/2,10-tk], [6,10], tk, roundEnd=2)]) :
  (id == 71) ? // G
    concat(
      [vLineTo([0,3.5], [tk,6.5], tk)],
      [hLineTo([3.5, 3.5], [7,3.5+tk], tk, roundEnd=1)],
      [arc([3.5, 6.5], 7, 7, 25, 180, tk, roundEnd=1)],
      [arc([3.5, 3.5], 7, 7, 180, 0, tk)]) :
  (id == 72) ? // H
    concat(
      [vLineTo([0,0], [tk,10], tk, roundEnd=3)],
      [vLineTo([7-tk,0], [7,10], tk, roundEnd=3)],
      [hLineTo([tk/2,5-tk/2], [7-tk/2,5+tk/2], tk)]) :
  (id == 73) ? // I
    [vLineTo([1-tk/2,0], [1+tk/2,10], tk, roundEnd=3)] :
  (id == 74) ? // J
    concat(
      [vLineTo([6-tk,3.5], [6,10], tk, roundEnd=2)],
      [arc([3, 3.5], 6, 7, 180, 0, tk, roundEnd=1)]) :
  (id == 75) ? // K
    concat(
      [vLineTo([0,0], [tk,10], tk, roundEnd = 3)],
      [vDiagLineTo([0,5], [7,10], tk, roundEnd = 2)],
      [vDiagLineTo([0,5], [7,0], tk, reverse = true, roundEnd = 2)]) :
  (id == 76) ? // L
    concat(
      [vLineTo([0,0], [tk,10], tk, roundEnd=2)],
      [hLineTo([tk/2,0], [6,tk], tk, roundEnd=2)]) :
  (id == 77) ? // M
    concat(
      [vLineTo([0,0], [tk,10], tk, roundEnd=1)],
      [vLineTo([8-tk,0], [8,10], tk, roundEnd=1)],
      [vDiagLineTo([0,10], [4,4], tk, halfEnd=true)],
      [vDiagLineTo([4,4], [8,10], tk, halfStart=true)]) :
  (id == 78) ? // N
    concat(
      [vLineTo([0,0], [tk,10], tk, roundEnd=1)],
      [vLineTo([7-tk,0], [7,10], tk, roundEnd=2)],
      [vDiagLineTo([0, 10], [7, 0], tk)]) :
  (id == 79) ? // O
    concat(
      [arc([4, 5], 8, 10, -1, 181, tk)],
      [arc([4, 5], 8, 10, 180, 360, tk)]) :
  (id == 80) ? // P
    concat(
      [vLineTo([0,0], [tk,10], tk, roundEnd=1)],
      [hLineTo([tk/2,5-tk/2],  [4,5+tk/2], tk)],
      [hLineTo([tk/2,10-tk], [4,10], tk)],
      [arc([4, 7.5-tk/4], 6, 5+tk/2, 270, 90, tk)]) :
  (id == 81) ? // Q
    concat(
      [arc([4, 5], 8, 10, 0, 360, tk)],
      [vDiagLineTo([3,4], [8,0], tk, reverse=true, roundEnd=3)]) :
  (id == 82) ? // R
    concat(
      [vLineTo([0,0], [tk,10], tk, roundEnd=1)],
      [hLineTo([tk/2,5-tk/2], [4,5+tk/2], tk)],
      [hLineTo([tk/2,10-tk], [4,10], tk)],
      [arc([4, 7.5-tk/4], 6, 5+tk/2, 270, 90, tk)],
      [vDiagLineTo([4, 5], [7, 0], tk, reverse=true, roundEnd=2)]) :
  (id == 83) ? // S
    concat(
      [arc([3.5, 10-(5+tk/2)/2], 7, 5+tk/2, 0, 270, tk, roundEnd=1)],
      [arc([3.5,   (5+tk/2)/2], 7, 5+tk/2, 180, 90, tk, roundEnd=1)]) :
  (id == 84) ? // T
    concat(
      [vLineTo([4-tk/2,0], [4+tk/2,10-tk], tk, roundEnd=1)],
      [hLineTo([0,10-tk], [8,10], tk, roundEnd=3)]) :
  (id == 85) ? // U
    concat(
      [vLineTo([0,3.5], [tk,10], tk, roundEnd=2)],
      [arc([3.5, 3.5], 7, 7, 180, 0, tk)],
      [vLineTo([7-tk,3.5], [7,10], tk, roundEnd=2)]) :
  (id == 86) ? // V
    concat(
      [vDiagLineTo([0,10], [3.5, 0], tk, halfEnd=true, roundEnd=1)],
      [vDiagLineTo([3.5, 0], [7,10], tk, halfStart=true, roundEnd=2)]) :
  (id == 87) ? // W
    let(hw = getHWidth(2.5, 8, tk),
        hw2 = getHWidth(2.5-hw, 14, tk))
    concat(
      [vDiagLineTo([0,10], [2.5,0], tk, roundEnd=1, halfEnd=true)],
      [vDiagLineTo([5.5,0], [8,10], tk, roundEnd=2, halfStart=true)],
      [vDiagLineTo([2.5,0], [4,7], tk, halfStart=true, halfEnd=true)],
      [vDiagLineTo([4,7], [5.5,0], tk, halfStart=true, halfEnd=true)]) :
  (id == 88) ? // X
    concat(
      [vDiagLineTo([0,0], [7,10], tk, roundEnd = 3)],
      [vDiagLineTo([0,10], [7,0], tk, roundEnd = 3)]) :
  (id == 89) ? // Y [not quite correct; shouldn't have a thicker stem]
    let(hw = getHWidth(7, 10, tk))
    concat(
      [vDiagLineTo([0,10], [3.5,5], tk, halfEnd=true, roundEnd=1)],
      [vDiagLineTo([3.5,5], [7,10], tk, halfStart=true, roundEnd=2)],
      [vLineTo([3.5-hw/2,0], [3.5+hw/2,5], hw, roundEnd=1)]) :
  (id == 90) ? // Z
    concat(
      [vDiagLineTo([0,tk], [7,10-tk], tk)],
      [hLineTo([0,10-tk], [7,10], tk, roundEnd=1)],
      [hLineTo([0,0], [7,tk], tk, roundEnd=2)]) :
  (id == 91) ? // [
    concat(
      [hLineTo([tk/2,0], [3,tk], tk, roundEnd=2)],
      [hLineTo([tk/2,10-tk], [3,10], tk, roundEnd=2)],
      [vLineTo([0,0], [tk,10], tk)]) :
  (id == 92) ? // \
    concat(
          [vDiagLineTo([0, 10], [6.5, 0], tk, reverse=true, roundEnd=3)]) :
  (id == 93) ? // ]
    concat(
      [hLineTo([0,0], [3-tk/2,tk], tk, roundEnd=1)],
      [hLineTo([0,10-tk], [3-tk/2,10], roundEnd=1)],
      [vLineTo([3-tk,0], [3,10], tk)]) :
  (id == 94) ? // ^
    concat(
      [vDiagLineTo([0,5], [3,9], tk, halfEnd=true, roundEnd=1)],
      [vDiagLineTo([3,9], [6,5], tk, halfStart=true, roundEnd=2)]) :
  (id == 95) ? // _
    concat(
      [hLineTo([0,0], [6.5,tk], tk)]) :
  (id == 96) ? // `
    concat(
          [vLineTo([2,6], [1, 9], tk/2)],
          [vLineTo([2,6], [1+tk/2, 9], tk/2)]) :
  (id == 97) ? // a
    concat(
          [vLineTo([5.5-tk,2], [5.5,3], tk)],
          [arc([2.5, 3], 6, 6, 0, 135, tk, roundEnd=2)],
          [arc([2.75, 2], 5.5, 4, 0, 360, tk)],
          [arc([6.5, 2], 2+2*tk, 4, 180, 270, tk, roundEnd=2)]) :
  (id == 98) ? // b
    concat(
          [vLineTo([0,3], [tk,10], tk, roundEnd=2)],
          [arc([3, 3], 6, 6, 0, 360, tk)]) :
  (id == 99) ? // c
    concat(
          [arc([3, 3], 6, 6, 30, 330, tk, roundEnd=3)]) :
  (id == 100) ? // d
    concat(
          [vLineTo([6-tk,2], [6,10], tk, roundEnd=2)],
          [arc([3, 3], 6, 6, 0, 360, tk)],
          [ arc([7, 2], 2+2*tk, 4, 180, 270, tk, roundEnd=2)]) :
  (id == 101) ? // e
    concat(
          [hLineTo([tk/2,3-tk/2], [6-tk/2,3+tk/2], tk)],
          [vLineTo([6-tk,3-tk/2], [6,3], tk)],
          [arc([3, 3], 6, 6, 0, 181, tk)],
          [arc([3, 3], 6, 6, 180, 315, tk, roundEnd=2)]) :
  (id == 102) ? // f
    concat(
          [vLineTo([0,0], [tk,7], tk, roundEnd=1)],
          [hLineTo([tk/2,6-tk], [3,6], tk, roundEnd=2)],
          [arc([3, 7], 6, 6, 60, 180, tk, roundEnd=1)]) :
  (id == 103) ? // g
    concat(
          [vLineTo([6-tk, 0], [6, 3], tk)],
          [arc([3, 0], 6, 6, 200, 360, tk, roundEnd=1)],
          [arc([3, 3], 6, 6, 0, 360, tk)]) :
  (id == 104) ? // h
    concat(
          [vLineTo([0,0], [tk,10], tk, roundEnd=3)],
          [vLineTo([6-tk,0], [6,3], tk, roundEnd=1)],
          [arc([3, 3], 6, 6, 0, 180, tk)]) :
  (id == 105) ? // i
    concat(
          [vLineTo([1-tk/2,0], [1+tk/2,6], tk, roundEnd=3)],
          [arc([1, 7.5], tk*1.5, tk*1.5, 0, 360, tk)]) :
  (id == 106) ? // j
    concat(
          [vLineTo([4-tk,0], [4,6], tk, roundEnd=2)],
          [arc([4-tk/2, 7.5], tk*1.5, tk*1.5, 0, 360, tk)],
          [arc([2, 0], 4, 4, 180, 360, tk, roundEnd=1)]) :
  (id == 107) ? // k
    concat(
      [vLineTo([0,0], [tk,10], tk, roundEnd=3)],
      [vDiagLineTo([0,3], [6,6], tk, roundEnd=2)],
      [vDiagLineTo([0,3], [6,0], tk, roundEnd=2)]) :
  (id == 108) ? // l
    concat(
          [vLineTo([0,2], [tk,10], tk, roundEnd=2)],
          [arc([2, 2], 4, 4, 180, 300, tk, roundEnd=2)]) :
  (id == 109) ? // m
    concat(
          [vLineTo([0,0], [tk,6], tk, roundEnd=3)],
          [vLineTo([4-tk/2,0], [4+tk/2,6-(4+tk/2)/2], tk, roundEnd=1)],
          [vLineTo([8-tk,0], [8,6-(4+tk/2)/2], tk, roundEnd=1)],
          [arc([(4+tk/2)/2, 6-(4+tk/2)/2], (4+tk/2), (4+tk/2), 0, 180, tk)],
          [arc([8-(4+tk/2)/2, 6-(4+tk/2)/2], (4+tk/2), (4+tk/2), 0, 180, tk)]) :
  (id == 110) ? // n
    concat(
          [vLineTo([0,0], [tk,6], tk, roundEnd=3)],
          [vLineTo([6-tk,0], [6,3], tk, roundEnd=1)],
          [arc([3, 3], 6, 6, 0, 180, tk)]) :
  (id == 111) ? // o
    concat(
          [arc([3, 3], 6, 6, 0, 360, tk)]) :
  (id == 112) ? // p
    concat(
          [vLineTo([0,-3], [tk,6], tk, roundEnd=3)],
          [arc([3, 3], 6, 6, -1, 181, tk)],
          [arc([3, 3], 6, 6, 180, 360, tk)]) :
  (id == 113) ? // q
    concat(
          [vLineTo([6-tk,-3], [6,6], tk, roundEnd=2)],
          [vDiagLineTo([6-tk,-3], [6.5+tk,-1], tk, roundEnd=2)],
          [arc([3, 3], 6, 6, 0, 360, tk)]) :
  (id == 114) ? // r
    concat(
          [vLineTo([0,0], [tk,6], tk, roundEnd=3)],
          [arc([3, 3], 6, 6, 60, 180, tk, roundEnd=1)]) :
  (id == 115) ? // s
    concat(
      [arc([2.5, 6-(3+tk/2)/2], 5, 3+tk/2, 0, 270, tk, roundEnd=1)],
      [arc([2.5,   (3+tk/2)/2], 5, 3+tk/2, 180, 90, tk, roundEnd=1)]) :
  (id == 116) ? // t
    concat(
          [vLineTo([0,2], [tk,10], tk, roundEnd=2)],
          [hLineTo([0,6-tk], [3,6], tk, roundEnd=2)],
          [arc([2, 2], 4, 4, 180, 300, tk, roundEnd=2)]) :
  (id == 117) ? // u
    concat(
          [vLineTo([0,3], [tk,6], tk, roundEnd=2)],
          [vLineTo([6-tk,2], [6,6], tk, roundEnd=2)],
          [arc([3, 3], 6, 6, 180, 360, tk)],
          [arc([7, 2], 2+2*tk, 4, 180, 270, tk, roundEnd=2)]) :
  (id == 118) ? // v
    concat(
      [vDiagLineTo([0,6], [3,0], tk, halfEnd=true, roundEnd=2)],
      [vDiagLineTo([3,0], [6,6], tk, halfStart=true, roundEnd=2)]) :
  (id == 119) ? // w
    concat(
      [vDiagLineTo([0,6], [2.5,0], tk, roundEnd=1, halfEnd=true)],
      [vDiagLineTo([5.5,0], [8,6], tk, roundEnd=2, halfStart=true)],
      [vDiagLineTo([2.5,0], [4,4], tk, halfStart=true, halfEnd=true)],
      [vDiagLineTo([4,4], [5.5,0], tk, halfStart=true, halfEnd=true)]) :
  (id == 120) ? // x
    concat(
      [vDiagLineTo([0,6], [6,0], tk, roundEnd=3)],
      [ vDiagLineTo([0,0], [6,6], tk, roundEnd=3)]) :
  (id == 121) ? // y
    concat(
          [vLineTo([0,3], [tk,6], tk, roundEnd=2)],
          [vLineTo([6-tk,0], [6,6], tk, roundEnd=2)],
          [arc([3, 0], 6, 6, 210, 360, tk, roundEnd=1)],
          [arc([3, 3], 6, 6, 180, 360, tk)]) :
  (id == 122) ? // z
    concat(
      [vDiagLineTo([0,tk], [5,6-tk], tk)],
      [hLineTo([0,6-tk], [5,6], tk, roundEnd=1)],
      [hLineTo([0,0], [5,tk], tk, roundEnd=2)]) :
  (id == 123) ? // {
    concat(
      [arc([3, 7.5-tk/4], 3+tk, 5+tk/2, 90, 180, tk, roundEnd=1)],
      [arc([0, 7.5-tk/4], 3+tk, 5+tk/2, 270, 0, tk, roundEnd=1)],
      [arc([0, 2.5+tk/4], 3+tk, 5+tk/2, 0, 90, tk, roundEnd=2)],
      [arc([3, 2.5+tk/4], 3+tk, 5+tk/2, 180, 270, tk, roundEnd=2)]) :
  (id == 124) ? // |
    concat(
      [vLineTo([1-tk/2,-1], [1+tk/2, 4-tk/2], tk, roundEnd=3)],
      [vLineTo([1-tk/2,4+tk/2], [1+tk/2, 9], tk, roundEnd=3)]) :
  (id == 125) ? // }
    concat(
      [arc([0, 7.5-tk/4], 3+tk, 5+tk/2, 0, 90, tk, roundEnd=2)],
      [arc([3, 7.5-tk/4], 3+tk, 5+tk/2, 180, 270, tk, roundEnd=2)],
      [arc([3, 2.5+tk/4], 3+tk, 5+tk/2, 90, 180, tk, roundEnd=1)],
      [arc([0, 2.5+tk/4], 3+tk, 5+tk/2, 270, 0, tk, roundEnd=1)]) :
  (id == 126) ? // ~
    concat(
      [arc([6.5 - (6.5+tk)/4, 5], (6.5+tk)/2, 4, 180, 360, tk, roundEnd=2)],
      [arc([(6.5+tk)/4, 5], (6.5+tk)/2, 4, 0, 180, tk, roundEnd=2)]) :
  (id == 196) ? // Ä
    concat(
      [arc([3-tk/2, 10-tk/2], tk, tk, 0, 360, tk)],
      [arc([4+tk/2, 10-tk/2], tk, tk, 0, 360, tk)],
      [vDiagLineTo([0,0], [3.5,8], tk, halfEnd=true, roundEnd=1)],
      [vDiagLineTo([3.5,8], [7, 0], tk, halfStart=true, roundEnd=2)],
      [hLineTo([(4-tk)*(2.5+tk/2)/10+tk/2,2.5], 
      [7-((4-tk)*(2.5+tk/2)/10+tk/2),2.5+tk], tk)]) :
  (id == 203) ? // Ë
    concat(
      [arc([2.5-tk/2, 10-tk/2], tk, tk, 0, 360, tk)],
      [arc([3.5+tk/2, 10-tk/2], tk, tk, 0, 360, tk)],
      [vLineTo([0,0], [tk,8], tk)],
      [hLineTo([tk/2,0], [6,tk], tk, roundEnd=2)],
      [hLineTo([tk/2,4-tk/2],  [4.75,4+tk/2], tk, roundEnd=2)],
      [hLineTo([tk/2,8-tk], [6,8], tk, roundEnd=2)]) :
  (id == 207) ? // Ï
    concat(
      [arc([1-tk/2, 10-tk/2], tk, tk, 0, 360, tk)],
      [arc([2+tk/2, 10-tk/2], tk, tk, 0, 360, tk)],
      [vLineTo([1.5-tk/2,0], [1.5+tk/2,8], tk, roundEnd=3)]) :
  (id == 214) ? // Ö
    concat(
      [arc([3-tk/2, 10-tk/2], tk, tk, 0, 360, tk)],
      [arc([4+tk/2, 10-tk/2], tk, tk, 0, 360, tk)],
      [arc([3.5, 4], 7, 8, 0, 360, tk)]) :
  (id == 220) ? // Ü
    concat(
      [arc([3-tk/2, 10-tk/2], tk, tk, 0, 360, tk)],
      [arc([4+tk/2, 10-tk/2], tk, tk, 0, 360, tk)],
      [vLineTo([0,3.5], [tk,8], tk, roundEnd=2)],
      [arc([3.5, 3.5], 7, 7, 180, 0, tk)],
      [vLineTo([7-tk,3.5], [7,8], tk, roundEnd=2)]) :
  (id == 223) ? // ß
    concat(
      [vLineTo([0,0], [tk,8-tk], tk, roundEnd=1)],
      [arc([2.25+tk/2, 8-tk], 4.5+tk, 4+2*tk, 0, 180, tk)],
      [arc([4.5, 8-(4+tk/2)/2], 5, 4+tk/2, 90, 270, tk)],
      [arc([4.5,    (4+tk/2)/2], 5, 4+tk/2, 210, 90, tk, roundEnd=1)]) :
  (id == 228) ? // ä
    concat(
      [arc([2.5-tk/2, 7.5], tk, tk, 0, 360, tk)],
      [arc([3.5+tk/2, 7.5], tk, tk, 0, 360, tk)],
      [vLineTo([5.5-tk,2], [5.5,3], tk)],
      [arc([2.5, 3], 6, 6, 0, 135, tk, roundEnd=2)],
      [arc([2.75, 2], 5.5, 4, 0, 360, tk)],
      [arc([6.5, 2], 2+2*tk, 4, 180, 270, tk, roundEnd=2)]) :
  (id == 235) ? // ë
    concat(
      [arc([2.5-tk/2, 7.5], tk, tk, 0, 360, tk)],
      [arc([3.5+tk/2, 7.5], tk, tk, 0, 360, tk)],
      [hLineTo([tk/2,3-tk/2], [6-tk/2,3+tk/2], tk)],
      [vLineTo([6-tk,3-tk/2], [6,3], tk)],
      [arc([3, 3], 6, 6, 0, 180, tk)],
      [arc([3, 3], 6, 6, 180, 315, tk)]) :
  (id == 239) ? // ï
    concat(
      [arc([1-tk/2, 7.5], tk, tk, 0, 360, tk)],
      [arc([2+tk/2, 7.5], tk, tk, 0, 360, tk)],
      [vLineTo([1.5-tk/2,0], [1.5+tk/2,6], tk, roundEnd=3)]) :
  (id == 246) ? // ö
    concat(
      [arc([2.5-tk/2, 7.5], tk, tk, 0, 360, tk)],
      [arc([3.5+tk/2, 7.5], tk, tk, 0, 360, tk)],
      [arc([3, 3], 6, 6, 0, 360, tk)]) :
  (id == 252) ? // ü
    concat(
      [arc([2.5-tk/2, 7.5], tk, tk, 0, 360, tk)],
      [arc([3.5+tk/2, 7.5], tk, tk, 0, 360, tk)],
      [vLineTo([0,3], [tk,6], tk, roundEnd=2)],
      [vLineTo([6-tk,2], [6,6], tk, roundEnd=2)],
      [arc([3, 3], 6, 6, 180, 360, tk)],
      [arc([8-tk, 2], 4, 4, 180, 270, tk, roundEnd=2)]) :
  (id == 256) ? // Ā
    concat(
      [hLineTo([1,10-tk], [6,10], tk, roundEnd=3)],
      [vDiagLineTo([0,0], [3.5,8], tk, halfEnd=true, roundEnd=1)],
      [vDiagLineTo([3.5,8], [7, 0], tk, halfStart=true, roundEnd=2)],
      [hLineTo([(4-tk)*(2.5+tk/2)/10+tk/2,2.5], 
      [7-((4-tk)*(2.5+tk/2)/10+tk/2),2.5+tk], tk)]) :
  (id == 257) ? // ā
    concat(
      [hLineTo([0.5,7.5-tk/2], [5.5,7.5+tk/2], tk, roundEnd=3)],
      [vLineTo([5.5-tk,2], [5.5,3], tk)],
      [arc([2.5, 3], 6, 6, 0, 135, tk, roundEnd=2)],
      [arc([2.75, 2], 5.5, 4, 0, 360, tk)],
      [arc([6.5, 2], 2+2*tk, 4, 180, 270, tk, roundEnd=2)]) :
  (id == 274) ? // Ē
    concat(
      [hLineTo([0.5,10-tk], [5.5,10], tk, roundEnd=3)],
      [vLineTo([0,0], [tk,8], tk)],
      [hLineTo([tk/2,0], [6,tk], tk, roundEnd=2)],
      [hLineTo([tk/2,4-tk/2],  [4.75,4+tk/2], tk, roundEnd=2)],
      [hLineTo([tk/2,8-tk], [6,8], tk, roundEnd=2)]) :
  (id == 275) ? // ē
    concat(
      [hLineTo([0.5,7.5-tk/2], [5.5,7.5+tk/2], tk, roundEnd=3)],
      [hLineTo([tk/2,3-tk/2], [6-tk/2,3+tk/2], tk)],
      [vLineTo([6-tk,3-tk/2], [6,3], tk)],
      [arc([3, 3], 6, 6, 0, 180, tk)],
      [arc([3, 3], 6, 6, 180, 315, tk, roundEnd=2)]) :
  (id == 298) ? // Ī
    concat(
      [hLineTo([0,10-tk], [3,10], tk, roundEnd=3)],
      [vLineTo([1.5-tk/2,0], [1.5+tk/2,8], tk, roundEnd=3)]) :
  (id == 299) ? // ī
    concat(
      [hLineTo([0,7.5-tk/2], [3,7.5+tk/2], tk, roundEnd=3)],
      [vLineTo([1.5-tk/2,0], [1.5+tk/2,6], tk, roundEnd=3)]) :
  (id == 332) ? // Ō
    concat(
      [hLineTo([1,10-tk], [6,10], tk, roundEnd=3)],
      [arc([3.5, 4], 7, 8, 0, 360, tk)]) :
  (id == 333) ? // ō
    concat(
      [hLineTo([0.5,7.5-tk/2], [5.5,7.5+tk/2], tk, roundEnd=3)],
      [arc([3, 3], 6, 6, 0, 360, tk)]) :
  (id == 362) ? // Ū
    concat(
      [hLineTo([1,10-tk], [6,10], tk, roundEnd=3)],
      [vLineTo([0,3.5], [tk,8], tk, roundEnd=2)],
      [arc([3.5, 3.5], 7, 7, 180, 0, tk)],
      [vLineTo([7-tk,3.5], [7,8], tk, roundEnd=2)]) :
  (id == 363) ? // ū
    concat(
      [hLineTo([0.5,7.5-tk/2], [5.5,7.5+tk/2], tk, roundEnd=3)],
      [vLineTo([0,3], [tk,6], tk, roundEnd=2)],
      [vLineTo([6-tk,2], [6,6], tk, roundEnd=2)],
      [arc([3, 3], 6, 6, 180, 360, tk)],
      [arc([8-tk, 2], 4, 4, 180, 270, tk, roundEnd=2)]) :
      [[]]; // fall-through

function lettersToNumbers(letters) =
  [for(i = [0:(len(letters)-1)]) (ord(letters[i]))];

function flatten(l) = 
  [ for (a = l) for (b = a) b ];

function cumSum(l, pos=0, sum=0, res=[]) =
  (pos >= len(l)) ? concat(res, [sum]) :
    cumSum(l, pos=pos+1, sum=sum+l[pos], res=concat(res, [sum]));

module mergePolyXOR(pointSets){
  if(len(pointSets) > 0){
    al = [for (a = pointSets) len(a)];
    acl = cumSum(al);
    pointPaths = [for (pi = [0:(len(pointSets)-1)]) 
                    [for(pii = [acl[pi]:(acl[pi+1]-1)]) pii]];
    echo(flatten(pointSets));
    echo(pointPaths);
    polygon(points=flatten(pointSets), paths=pointPaths);
  }
}

module mergePolyUnion(pointSets){
  if(len(pointSets) > 0){
    for (pi = [0:(len(pointSets)-1)]){
     polygon(pointSets[pi]);
    }
  }
}

module printLetterPoints(letterArray, gap=0.5, centre=false, 
                         showBlocks=false, tk=1.5){
	translate([(centre) ? -(sumLwds(letterArray,0,
                           len(letterArray)-1) + 
                          (len(letterArray)-1)*gap) / 2 : 0,0]) {
    for(i = [0:(len(letterArray)-1)]){
      //echo(ltrPoints(letterArray[i])[0][0]);
      translate([sumLwds(letterArray,0,i-1)+i*gap,0]) 
        mergePolyUnion(ltrPoints(letterArray[i], tk=tk));
    }
  }
}

// test rig for debugging letters
*union(){
  myLetter="R";
  myWidth=gW(lettersToNumbers(myLetter)[0]);
  translate([-myWidth/2,-5,0]){ 
    linear_extrude(height=0.5){
      mergePolyUnion(ltrPoints(lettersToNumbers(myLetter)[0]));
    }
    *linear_extrude(height=0.25){
      ltr(lettersToNumbers(myLetter)[0]);
    }
  }
}

*color("pink") linear_extrude(height=2, center=true){
  letThick=(sin($t*360)+1)/1.5+0.5;
  translate([0,10])
    printLetterPoints(lettersToNumbers("āēīōū ĀĒĪŌŪ"),
       centre=true, tk=letThick, gap=1);
  translate([0,-5])
    printLetterPoints(lettersToNumbers("äëïöü ÄËÏÖÜß"),
       centre=true, tk=letThick, gap=1);
}

*color("darkorange") //linear_extrude(height=2, center=true)
  translate([0, 30]){
  letThick=(sin($t*360)+1)/1.5+0.5;
	translate([0,0]) 
    printLetterPoints(lettersToNumbers("“SPHINX OF BLACK QUARTZ”"),
       centre=true, tk=letThick, gap=1);
	translate([0,-12]) 
    printLetterPoints(lettersToNumbers("‘JUDGE MY VOW?’"),
     centre=true, tk=letThick, gap=1);
	translate([0,-30]) 
    printLetterPoints(lettersToNumbers("{sphinx of black quartz}"),
     centre=true, tk=letThick, gap=1);
	translate([0,-41]) 
    printLetterPoints(lettersToNumbers("[judge ~ my | vow]"),
     centre=true, tk=letThick, gap=1);
	translate([0,-60]) 
    printLetterPoints(lettersToNumbers("<1234567890> \\ !@#$%^&*()"),
     centre=true, tk=letThick, gap=1);
	translate([0,-80]) 
    printLetterPoints(lettersToNumbers("4:30~8:20; 2+3=5, ...5-3=2"),
     centre=true, tk=letThick, gap=1);
	translate([0,-100]) 
    printLetterPoints(lettersToNumbers("ÄĀäāËĒëēÏĪïīÖŌöōÜŪüūß"),
     centre=true, tk=letThick, gap=1);
}

color("white") //linear_extrude(height=2, center=true)
  translate([0, 30]){
  letThick=(sin($t*360)+1)/1.5+0.75;
	translate([0,0]) 
    printLetterPoints(lettersToNumbers("If you like my work,"),
     centre=true, tk=letThick, gap=1);
	translate([0,-13]) 
    printLetterPoints(lettersToNumbers("Please check out my website:"),
     centre=true, tk=letThick, gap=1);
	translate([-72,-26]) // manual kerning
     printLetterPoints(lettersToNumbers("https:/"),
     centre=false, tk=letThick, gap=1);
	translate([-37,-26]) 
     printLetterPoints(lettersToNumbers("/"),
     centre=false, tk=letThick, gap=1);
	translate([-31,-26]) 
    printLetterPoints(lettersToNumbers("www.gringene.org"),
     centre=false, tk=letThick, gap=1);
  }
translate([0,0,-2]) color("#C500FF") cube([300,300,1], center=true);


*color("darkorange") printLetterPoints(lettersToNumbers("Gk*wG"),
  tk=(sin($t*360)+1)/1.5+0.5 , centre=true);

echo(ord("‘"),ord("’"));

*printLetterPoints(lettersToNumbers("T*F"), tk=(0.5 + sin($t * 180)) , centre=true);

*printLetterPoints(lettersToNumbers("T"), tk=(0.5 + sin($t * 180)) , centre=true);
*printLetterPoints(lettersToNumbers("F"), tk=(0.5 + sin($t * 180)) , centre=true);

//translate([0,0,-1]) printLetterPoints(lettersToNumbers("p"));

*for(sp = [0:1:8]){
  translate([sp,-3]) square([0.5,13]);
}

*for(sp = [-3:1:10]){
  translate([0,sp]) square([8,0.5]);
}
