#!/usr/bin/env Rscript

library(tidyverse)

data.tbl <- read_csv("kernShadows.csv") %>%
  rbind(tibble(id=32, name="space", width=6.5,
               leftShadow=paste(rep(0,101), collapse=";"),
               rightShadow=paste(rep(65,101), collapse=";")))

pdf("kernShadows.pdf", width=11, height=8);
par(mfrow=c(3,4), mar=c(2,2,3,0.5))
for(r in seq_len(nrow(data.tbl))){
  plot(NA, xlim=c(0, 8.5), ylim=c(0, 10),
       main = sub("^(LATIN)?", sprintf("[%d] ", data.tbl$id[r]), data.tbl$name[r]));
  leftShadow = as.numeric(unlist(strsplit(data.tbl$leftShadow[r], ";")))/10;
  rightShadow = as.numeric(unlist(strsplit(data.tbl$rightShadow[r], ";")))/10;
  points(leftShadow, 0:100 / 10, pch=19, 
         col=ifelse((leftShadow <= 0) | (leftShadow == 8.1), "darkgreen", "black"));
  points(rightShadow, 0:100 / 10, pch=19, 
         col=ifelse((rightShadow >= data.tbl$width[r]) |
                      (rightShadow == -0.1),
                    "blue", "black"));
  abline(v=c(0, data.tbl$width[r]), lty = "dotted");
}
invisible(dev.off());

plotStr <-
  sapply(unlist(strsplit("Today kerning", "")),
                         function(x){as.integer(charToRaw(x))});

shadowRows <- match(plotStr, data.tbl$id);
fullLen <- 0.5;
for(sr in shadowRows){
  fullLen <- fullLen + data.tbl$width[sr] + 0.5;
}
par(mar=c(2,2,0.5,0.5));
plot(NA, xlim=c(0, fullLen), ylim=c(0, 10));
cumLen <- 0.5;
colvals <- c("orangered", "blue");
cvi <- 1;
for(sr in shadowRows){
  leftShadow = as.numeric(unlist(strsplit(data.tbl$leftShadow[sr], ";")))/10;
  leftShadow[leftShadow == 8.1] <- NA;
  rightShadow = as.numeric(unlist(strsplit(data.tbl$rightShadow[sr], ";")))/10;
  rightShadow[rightShadow == -0.1] <- NA;
  segments(x0=cumLen + leftShadow, y0=0:100 / 10,
           x1=cumLen + rightShadow, col=colvals[cvi]);
  cvi <- (cvi %% 2) + 1;
  cumLen <- cumLen + data.tbl$width[sr] + 0.5;
}
